(function(){
	var requireDir = require('require-dir');
	requireDir('../gulp', { recurse: true });

	var gulp = require('gulp')
		, bodyParser = require('body-parser')
	    , gulpLoadPlugins = require('gulp-load-plugins')
	    , plugins = gulpLoadPlugins()
	    , path = require('path')
	    , paths = require('../settings/config').gulpPaths
	    , data = require('../../models/data')
	    , gutil = require('gulp-util')
		, exec = require('child_process').exec
		, htmlhint_inline = require('gulp-htmlhint-inline');

	// var config = require('../../../pcs-config.json');
	var fs = require('fs');

	module.exports = function(app) {

		app.use(bodyParser.urlencoded({ extended: false }));
		app.use(bodyParser.json());

		app.post('/getconfig', function(req, res){
			delete require.cache[require.resolve('../../../pcs-config.json')];
			config = require('../../../pcs-config.json');
			res.send(config);
		});

		app.post('/setconfig', function(req, res){
			var data = req.body;
			fs.writeFile('./pcs-config.json', JSON.stringify(data), function (err) {
			  if(err) {
			  	res.send(err);
			  } else {
			  	res.send({
			  		return: 'success'
			  	});
			  }
			});
		});

		/* POST */
		app.post('/generate', function(req, res){
			
			var ret = {};
			var generateType = req.body.type;
			var generateStep = req.body.step;

			if(generateType === 'css') {
				if(generateStep == 1) {
					gulp.start('sass', function(){
						ret = {
							return: 'success',
							type: 'css', 
							step: generateStep
						};
						gutil.log(gutil.colors.green('CSS combined and distributed to /dist'));
						res.send(ret);
					});
				}
				if(generateStep == 2) {
					gulp.start('css-optimize', function(){
						// copy - css
						gulp.src(paths.css.dest + '/*.*')
					    .pipe(gulp.dest(paths.productionPath.css))
					    .on('end', function(){
					    	ret = {
					    		return: 'success',
					    		type: 'css',
					    		step: generateStep,		    
					    		file: 'style.min.css',		
					    		path: paths.productionPath.css
					    	};
							gutil.log(gutil.colors.cyan('Production CSS has been generated from pcs'));
							res.send(ret);
					    })
					    .on('error', function(error){
					    	res.send(error);
					    });
					});
				}	
			}

			if(generateType === 'js') {
				// gutil.log('generate js starts ' + generateStep);

				if(generateStep == 1) {
					gulp.start('js', function(){
						ret = {
							return: 'success',
							type: 'js', 
							step: generateStep
						};
						gutil.log(gutil.colors.green('JS combined and distributed to /dist'));
						res.send(ret);
					});
				}
				if(generateStep == 2) {
					// gutil.log('js-optimize' + generateStep);

					gulp.start('js-optimize', function(){
						gulp.src(paths.js.dest + '/*.*')
					    .pipe(gulp.dest(paths.productionPath.js))
					    .on('end', function(){
					    	ret = {
					    		return: 'success',
					    		type: 'js',
					    		step: generateStep,		    
					    		file: 'site.min.js',		
					    		path: paths.productionPath.js
					    	};
							gutil.log(gutil.colors.cyan('Production JS has been generated from pcs'));
							res.send(ret);
					    })
					    .on('error', function(error){
					    	res.send(error);
					    });
					});
				}	
			}

			if(generateType === 'html') {
				if(generateStep == 1) {
					gulp.start('ejs', function(){
						ret = {
							return: 'success',
							type: 'html', 
							step: generateStep
						};
						gutil.log(gutil.colors.green('EJS distributed to /dist as *.HTML'));
						res.send(ret);
					});
				}
				if(generateStep == 2) {
					// copy - html
					gulp.src(paths.ejs.src + 'index*.ejs')
					.pipe(plugins.ejs({
						production: true
						, lang: (plugins.util.env.lang ? plugins.util.env.lang : 'en')
						, region: (plugins.util.env.region ? plugins.util.env.region : 'US')
						, mobile: plugins.util.env.mobile 
						, data: data
					}))
					.pipe(plugins.rename({suffix: '-content', extname: '.html'}))
					.pipe(plugins.removeEmptyLines())
					.pipe(gulp.dest(paths.productionPath.html))
					.on('end', function(){
				    	ret = {
				    		return: 'success',
				    		type: 'html',
				    		step: generateStep,		    
				    		file: '*.html',		
				    		path: paths.productionPath.html
				    	};
						gutil.log(gutil.colors.cyan('Production HTMLs have been generated from pcs'));
						res.send(ret);
				    })
				    .on('error', function(error){
				    	res.send(error);
				    });

				}	
			}
		});
		app.post('/deploy', function(req, res){
			
			var ret = {};
			var generateType = req.body.type;

			if(generateType === 'css') {
				gulp.start('deploy-stg-css', function(){
					ret = {
						return: 'success',
						type: 'css',
						file: 'style.min.css',
						path: paths.sftp.stage.dest.css
					};
					gutil.log(gutil.colors.green('CSS deployed to stage11'));
					res.send(ret);
				});
			}

			if(generateType === 'js') {
				gulp.start('deploy-stg-js', function(){
					ret = {
						return: 'success',
						type: 'js',
						file: 'site.min.js',
						path: paths.sftp.stage.dest.js
					};
					gutil.log(gutil.colors.green('JS deployed to stage11'));
					res.send(ret);
				});
			}
		});
		app.post('/show', function(req, res){
			var type = req.body.type;
			if(type === 'css') {
				exec('open production/css/');
				res.send();
			}
			if(type === 'html') {
				exec('open production/html/');
				res.send();
			}
			if(type === 'js') {
				exec('open production/js/');
				res.send();
			}
		});
		app.post('/validate', function(req, res){
			var type = req.body.type;
			if(type === 'css') {
				var result = '';
				var resultGen = function(file){
					var returnVal = '';
					var returnBool = false;

					returnVal += '<div style="overflow: auto;" class="val-css-file">'
					returnVal += '<a style="color:red; cursor:pointer;" onclick="valDetails(this);"><strong>'
					returnVal += file.path;
					returnVal += '</strong></a></div>';
					returnVal += '<div style="display:none">';
					file.csslint.results.forEach(function(res) {
						// scss lint exceptions
						if(res.error.message.startsWith('Expected ')) return;
						if(res.error.message.startsWith('Unknown @ rule: @mixin')) return;
						if(res.error.message.startsWith("Unexpected token ")) return;

						returnVal += res.error.message + ' ' + res.error.line + '<br/>';

						returnBool = true;
					});
					returnVal += '</div>';

					if(returnBool) result += returnVal;
				}

				gulp.src(paths.sass.src)
				.pipe(plugins.csslint('.csslintrc'))
				.pipe(plugins.csslint.reporter(resultGen))		
				.on('finish', function(){
					var ret = {
						type: type,
						return: result
					}
					res.send(ret);
				});
			}
			if(type === 'html') {
				var result = '';
				var resultGen = function(file){

					if (!file.htmlhint_inline.success) {
			            // console.log('custom reporter: htmlhint-inline fail in '+ file.path);
			        }

					var returnVal = '';
					var returnBool = false;

					returnVal += '<div style="overflow: auto;" class="val-css-file">'
					returnVal += '<a style="color:red; cursor:pointer;" onclick="valDetails(this);"><strong>'
					returnVal += file.path;
					returnVal += '</strong></a></div>';
					returnVal += '<div style="display:none">';
					file.htmlhint_inline.forEach(function(data){
						returnVal += data.rule.description + ' Line: ' + data.line + '<br/>';
						returnBool = true;
					});
					returnVal += '</div>';

					if(returnBool) result += returnVal;
				}
				// Currently, options can not be used because of the htmlhint_inline's bug
				var htmlhintOptions = {
			        // htmlhintrc: ".htmlhintrc",
			        ignores: {
			          '<%': '%>'
			        }
			    };
				gulp.src(paths.productionPath.html + '/*')
				.pipe(htmlhint_inline(htmlhintOptions))
				.pipe(htmlhint_inline.reporter(resultGen))
				.on('finish', function(){
					var ret = {
						type: type,
						return: result
					}
					res.send(ret);
				});
			}
			if(type === 'js') {
				var result = '';
				var resultGen = function(file){
					var returnVal = '';
					var returnBool = false;

					returnVal += '<div style="overflow: auto;" class="val-css-file">'
					returnVal += '<a style="color:red; cursor:pointer;" onclick="valDetails(this);"><strong>'
					returnVal += file[0].file;
					returnVal += '</strong></a></div>';
					returnVal += '<div style="display:none">';
					file.forEach(function(data){
						returnVal += data.error.reason + ' Line: ' + data.error.line + '<br/>';
						returnBool = true;
					});
					returnVal += '</div>';

					if(returnBool) result += returnVal;
				}

				gulp.src(paths.js.src)
				.pipe(plugins.jshint())
				.pipe(plugins.jshint.reporter(resultGen))
				.on('finish', function(){
					var ret = {
						type: type,
						return: result
					}
					res.send(ret);
				});
			}

		});

	}
})();