'use strict';

var gulp = require('gulp')
    , env = process.env.NODE_ENV || 'development';

var argv = require('yargs').argv;

gulp.task('default', ['clean'], function(defaultTasks){
	// gulp.start(env);

	var mode = argv.mode == 'responsive' ? 'responsive':'development';
	if(mode == 'responsive') {
		// process.env.NODE_ENV = JSON.stringify("production");
		gulp.start(mode);
	} else {
		gulp.start(env);
	}
});



