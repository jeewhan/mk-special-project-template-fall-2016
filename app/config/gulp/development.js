var gulp = require('gulp')
    , paths = require('../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , gutil = require('gulp-util');

gulp.task('development', [ 
	'sass'
	, 'sassDev'
	, 'css'
	, 'js'
	, 'jsDev'
	, 'imgs'
	, 'imgsDev'
	, 'videos'
	, 'html'
	, 'ejs'
    , 'fonts'
    , 'fontsDev'
	, 'devServe'
    , 'watch'
], function(){
    gulp.start('global-assets');
});

gulp.task('global-assets', ['sass-global', 'css-global', 'js-global', 'imgs-global', 'fonts-global']);

gulp.task('browserSync', function(){
    browserSync(paths.browserSync);
});

gulp.task('watch', ['browserSync'], function(){
    gulp.watch(paths.js.src, ['js']);
    gulp.watch(paths.sass.src, ['sass']);
    gulp.watch(paths.imgs.src, ['imgs']);
    gulp.watch(paths.html.src, ['html']);
    gulp.watch(paths.ejs.src, ['ejs']);
    gulp.watch(paths.fonts.src, ['fonts']);
    gulp.watch(paths.videos.src, ['videos']);
});