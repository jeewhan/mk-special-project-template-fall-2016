var gulp = require('gulp')
    , paths = require('../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , gutil = require('gulp-util')
    , reload = browserSync.reload;

gulp.task('responsive', [ 
	'sass'
	, 'sassDev:responsive'
	, 'css'
	, 'js'
	, 'jsDev:responsive'
	, 'imgs'
	, 'imgsDev'
	, 'videos'
	, 'html'
	, 'ejs'
    , 'fonts'
    , 'fontsDev'
    , 'fontsDev:responsive'
	, 'devServe:responsive'
    , 'watch:responsive'
], function(){
    gulp.start('global-assets');
});

gulp.task('watch:responsive', ['browserSync'], function(){
    gulp.watch(paths.js.src, ['js']);
    gulp.watch(paths.sass.src, ['sass']);
    gulp.watch(paths.imgs.src, ['imgs']);
    gulp.watch(paths.html.src, ['html']);
    gulp.watch(paths.ejs.src, ['ejs']);
    gulp.watch(paths.fonts.src, ['fonts']);
    gulp.watch(paths.videos.src, ['videos']);
});

gulp.task('sassDev:responsive', function() {
    return gulp.src(paths.sassDevResponsive.src)
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sass())
        .pipe(plugins.concat('style.css'))
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.sourcemaps.write())
        .pipe(gulp.dest(paths.sassDev.dest))
        .pipe(reload({stream:true}));;
});

gulp.task('jsDev:responsive', function () {
    return gulp.src(paths.jsDevResponsive.src)
    .pipe(plugins.concat('site.js'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.jsDev.dest))
    .pipe(reload({stream:true}));
});

gulp.task('fontsDev:responsive', function () {
    return gulp.src(paths.fontsDevResponsive.src)
    .pipe(gulp.dest(paths.fontsDevResponsive.dest))
    .pipe(reload({stream:true}));
});

gulp.task('devServe:responsive', function(cb) {

    gutil.log('devServe:responsive Starts');
    
    var started = false;
    
    return plugins.nodemon(paths.nodemon.settingsResponsive).on('start', function () {
        if (!started) {
            cb();
            started = true; 
        } 
    }).on('restart', function onRestart() {
        setTimeout(function reload() {
              browserSync.reload({
                stream: false //
              });
        }, 500);
     });

    // cb();
});