var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('imgs', function () {
	return gulp.src(paths.imgs.src)
		.pipe(gulp.dest(paths.imgs.dest))
		.pipe(reload({stream:true}));
});

gulp.task('imgs-global', ['imgs-ca-fr', 'imgs-de', 'imgs-es', 'imgs-fr', 'imgs-it'], function(){
	return;
});

gulp.task('imgsDev', function () {
	return gulp.src(paths.imgsDev.src)
		.pipe(gulp.dest(paths.imgsDev.dest))
		.pipe(reload({stream:true}));
});

gulp.task('imgs-ca-fr', function () {
	return gulp.src(paths.imgsCA_FR.src)
		.pipe(gulp.dest(paths.imgsCA_FR.dest))
		.pipe(reload({stream:true}));
});
gulp.task('imgs-de', function () {
	return gulp.src(paths.imgsDE.src)
		.pipe(gulp.dest(paths.imgsDE.dest))
		.pipe(reload({stream:true}));
});
gulp.task('imgs-es', function () {
	return gulp.src(paths.imgsES.src)
		.pipe(gulp.dest(paths.imgsES.dest))
		.pipe(reload({stream:true}));
});
gulp.task('imgs-fr', function () {
	return gulp.src(paths.imgsFR.src)
		.pipe(gulp.dest(paths.imgsFR.dest))
		.pipe(reload({stream:true}));
});
gulp.task('imgs-it', function () {
	return gulp.src(paths.imgsIT.src)
		.pipe(gulp.dest(paths.imgsIT.dest))
		.pipe(reload({stream:true}));
});