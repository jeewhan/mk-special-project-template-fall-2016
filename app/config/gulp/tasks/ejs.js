var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , data = require('../../../models/data')
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('ejs', function () {
	// return gulp.src(paths.ejs.src + 'index.ejs')
	return gulp.src(paths.ejs.src)
		// .pipe(plugins.ejs({
			// production: plugins.util.env.production
			// , lang: (plugins.util.env.lang ? plugins.util.env.lang : 'en')
			// , region: (plugins.util.env.region ? plugins.util.env.region : 'US')
			// , mobile: plugins.util.env.mobile
			// , data: data
		// }))
		// .pipe(plugins.rename({extname: '.html'}))
		// .pipe(gulp.dest(paths.ejs.dest))
		.pipe(reload({stream:true}));
});
