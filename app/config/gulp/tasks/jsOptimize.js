var gulp = require('gulp')
	  , gulpLoadPlugins = require('gulp-load-plugins')
	  , plugins = gulpLoadPlugins()
	  , settings = require('../../settings/config')
    , paths = settings.gulpPaths;

gulp.task('js-optimize', function () {
   return gulp.src(paths.js.dest+'/site.min.js')
		.pipe(plugins.jshint('.jshintrc'))
		.pipe(plugins.uglify())
		.pipe(gulp.dest(paths.js.dest));
});