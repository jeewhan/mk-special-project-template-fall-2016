var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
		, browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('html', function () {
	return gulp.src(paths.html.src)
		.pipe(reload({stream:true}));
});