var gulp = require('gulp')
	, fs = require('fs')
    , paths = require('../../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , gutil = require('gulp-util')
    , plugins = gulpLoadPlugins();

var config = {
	host: paths.sftp.stage.host,
	port: paths.sftp.stage.port,
	username: paths.sftp.stage.username
	// privateKey: fs.readFileSync(paths.sftp.privateKey)
};

var gulpSSH = new plugins.ssh({
	ignoreErrors: false,
	sshConfig: config
});

gulp.task('deploy-stg-css', function () {
  return gulp.src(paths.productionPath.css + '/*.css')
    .pipe(gulpSSH.dest(paths.sftp.stage.dest.css));
});

gulp.task('deploy-stg-js', function () {
  return gulp.src(paths.productionPath.js + '/*.js')
    .pipe(gulpSSH.dest(paths.sftp.stage.dest.js));
});

gulp.task('deploy-stg-check', function (cb) {
  gulpSSH.sftp('read', paths.sftp.stage.dest.css+'/style.min.css')
    .on('error',function(e){
      gutil.log('error: no such file');
      cb();
    })
    .on('finish', function(){
      gutil.log('success');
      cb();
    })   
});