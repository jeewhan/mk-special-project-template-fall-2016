var gulp = require('gulp')
    , del = require('del');

gulp.task('clean', function(cb) {
	return del(['./dist/**/*', './dist/assets', './dist/*.html'], cb);
});

gulp.task('cleanProd', function(cb) {
	return del(['./production'], cb);
});