var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('fonts', function () {
	return gulp.src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest))
    .pipe(reload({stream:true}));
});

gulp.task('fontsDev', function () {
	return gulp.src(paths.fontsDev.src)
    .pipe(gulp.dest(paths.fontsDev.dest))
    .pipe(reload({stream:true}));
});

gulp.task('fonts-global', function () {
    gulp.src(paths.fonts.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsCA_FR/css/fonts'));
    gulp.src(paths.fonts.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsDE/css/fonts'));
    gulp.src(paths.fonts.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsES/css/fonts'));
    gulp.src(paths.fonts.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsFR/css/fonts'));
    return gulp.src(paths.fonts.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsIT/css/fonts'));
});
