var gulp = require('gulp')
	  , gulpLoadPlugins = require('gulp-load-plugins')
	  , plugins = gulpLoadPlugins()
	  , settings = require('../../settings/config')
    , paths = settings.gulpPaths;

gulp.task('imgs-optimize', function () {
	return gulp.src(paths.imgs.src)
		.pipe(plugins.imagemin({optimizationLevel: 8}))
		.pipe(gulp.dest(paths.imgs.dest));
});