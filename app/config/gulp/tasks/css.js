var gulp = require('gulp')
		, paths = require('../../settings/config').gulpPaths
		, gulpLoadPlugins = require('gulp-load-plugins')
		, plugins = gulpLoadPlugins()
		, browserSync = require('browser-sync')
		, sassLint = require('gulp-sass-lint')
		, reload = browserSync.reload;

gulp.task('css', function() {
	return gulp.src(paths.css.src)
		.pipe(plugins.concat('style.css'))
		.pipe(plugins.rename({suffix: '.min'}))
		.pipe(gulp.dest(paths.css.dest))
		.pipe(reload({stream:true}));;
});

gulp.task('css-global', function() {
	gulp.src(paths.css.dest+'/**/*')
	.pipe(gulp.dest('./dist/assetsCA_FR/css'))
	gulp.src(paths.css.dest+'/**/*')
	.pipe(gulp.dest('./dist/assetsDE/css'))
	gulp.src(paths.css.dest+'/**/*')
	.pipe(gulp.dest('./dist/assetsES/css'))
	gulp.src(paths.css.dest+'/**/*')
	.pipe(gulp.dest('./dist/assetsFR/css'))
	return gulp.src(paths.css.dest+'/**/*')
	.pipe(gulp.dest('./dist/assetsIT/css'))
});

gulp.task('css-lintrc', function () {
   var ret = gulp.src(paths.sass.src)
		.pipe(plugins.csslint('.csslintrc'))
		.pipe(plugins.csslint.reporter('junit-xml', {logger: function(str) { output += str; }}));

	return ret;
		// .on('finish', function(te){
			// plugins.util.log(te);
		// })
		// .pipe(plugins.csslint.result())
		// .pipe(plugins.util.log(plugins.csslint.errorCount))

		// return gulp.src(paths.sass.src)
		// .pipe(plugins.sourcemaps.init())
		// .pipe(plugins.sass())
		// .pipe(plugins.autoprefixer({ browsers: ['last 2 version'] }))
		// .pipe(plugins.concat('style.css'))
		// .pipe(plugins.rename({suffix: '.min'}))
		// .pipe(plugins.sourcemaps.write())
		// .pipe(gulp.dest(paths.sass.dest))
		// .pipe(reload({stream:true}));;
});

var myFunc = function(file){
	plugins.util.log('test');
}

gulp.task('scss-lint', function(){
	plugins.util.log('test0');

	return gulp.src(paths.sass.src)
		.pipe(sassLint({
			options: {
		        formatter: 'stylish'
		    }			
		}))
	    .pipe(sassLint.format())
	    // .pipe(sassLint.failOnError())
});