var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserify      = require('browserify')
    , browserSync = require('browser-sync')
    , gutil = require('gulp-util');

gulp.task('devServe', function(cb) {

    gutil.log('devServe Starts');
    
    var started = false;
    
    // nodemon -> browserify
    // var app = browserify([paths.js.src + '/main.js'], {debug: true});
    // var watch = watchify(app);
    
    return plugins.nodemon(paths.nodemon.settings).on('start', function () {
        // to avoid nodemon being started multiple times
        if (!started) {
            cb();
            started = true; 
        } 
    }).on('restart', function onRestart() {
        // reload connected browsers after a slight delay
        setTimeout(function reload() {
              browserSync.reload({
                stream: false //
              });
        }, 500);
     });

    // cb();
});