var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('pcs', [ 
	'pcs-js'
	, 'pcs-scss'
	, 'pcs-ejs'
	, 'pcs-devServe'
	, 'pcs-watch'
]);

gulp.task('pcs-js', function () {
	return gulp.src(paths.pcs.js.src)
    .pipe(gulp.dest(paths.pcs.js.dest))
    .pipe(reload({stream:true}));
});

gulp.task('pcs-scss', function() {
	return gulp.src(paths.pcs.css.src)
		.pipe(plugins.sourcemaps.init())
		.pipe(plugins.sass())
		.pipe(plugins.sourcemaps.write())
		.pipe(gulp.dest(paths.pcs.css.dest))
		.pipe(reload({stream:true}));
});

gulp.task('pcs-ejs', function () {
	return gulp.src(paths.ejs.src + 'pcs/**')
		.pipe(plugins.ejs())
		.pipe(plugins.rename({extname: '.html'}))
		.pipe(gulp.dest(paths.ejs.dest))
		.pipe(reload({stream:true}));
});

gulp.task('pcs-watch', function(){
    gulp.watch(paths.pcs.js.src, ['pcs-js']);
    gulp.watch(paths.pcs.css.src, ['pcs-scss']);
    gulp.watch(paths.ejs.src+'pcs/**', ['pcs-ejs']);
});

gulp.task('pcs-devServe', function (cb) {
    
    var started = false;
    
    return plugins.nodemon(paths.nodemon.settings).on('start', function () {
        // to avoid nodemon being started multiple times
        if (!started) {
            cb();
            started = true; 
        } 
    }).on('restart', function onRestart() {
        // reload connected browsers after a slight delay
        setTimeout(function reload() {
              browserSync.reload({
                stream: false //
              });
        }, 500);
     });
});