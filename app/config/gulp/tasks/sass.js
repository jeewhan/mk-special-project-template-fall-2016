var gulp = require('gulp')
		, paths = require('../../settings/config').gulpPaths
		, gulpLoadPlugins = require('gulp-load-plugins')
		, plugins = gulpLoadPlugins()
		, browserSync = require('browser-sync')
		, reload = browserSync.reload;

gulp.task('sass', function() {
	return gulp.src(paths.sass.src)
		.pipe(plugins.sourcemaps.init())
		.pipe(plugins.sass())
		.pipe(plugins.autoprefixer({ browsers: ['last 2 version'] }))
		.pipe(plugins.concat('style.css'))
		.pipe(plugins.rename({suffix: '.min'}))
		.pipe(plugins.sourcemaps.write())
		.pipe(gulp.dest(paths.sass.dest))
		.pipe(reload({stream:true}));
});

gulp.task('sassDev', function() {
	return gulp.src(paths.sassDev.src)
		.pipe(plugins.sourcemaps.init())
		.pipe(plugins.sass())
		.pipe(plugins.concat('style.css'))
		.pipe(plugins.rename({suffix: '.min'}))
		.pipe(plugins.sourcemaps.write())
		.pipe(gulp.dest(paths.sassDev.dest))
		.pipe(reload({stream:true}));;
});

gulp.task('sass-global', function(){
	gulp.src(paths.sass.dest + '/**/*')
		.pipe(gulp.dest('./dist/assetsCA_FR/css'));
	gulp.src(paths.sass.dest + '/**/*')
		.pipe(gulp.dest('./dist/assetsDE/css'));
	gulp.src(paths.sass.dest + '/**/*')
		.pipe(gulp.dest('./dist/assetsES/css'));
	gulp.src(paths.sass.dest + '/**/*')
		.pipe(gulp.dest('./dist/assetsFR/css'));
	return gulp.src(paths.sass.dest + '/**/*')
		.pipe(gulp.dest('./dist/assetsIT/css'));
});