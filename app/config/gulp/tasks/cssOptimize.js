var gulp = require('gulp')
	  , gulpLoadPlugins = require('gulp-load-plugins')
	  , plugins = gulpLoadPlugins()
	  , settings = require('../../settings/config')
    , paths = settings.gulpPaths;

gulp.task('css-optimize', function () {
   return gulp.src(paths.css.dest+'/style.min.css')
		// .pipe(plugins.csslint('.csslintrc'))
		.pipe(plugins.cssmin())
		.pipe(gulp.dest(paths.css.dest));
});