var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('videos', function () {
	return gulp.src(paths.videos.src)
		.pipe(gulp.dest(paths.videos.dest))
		.pipe(reload({stream:true}));
});