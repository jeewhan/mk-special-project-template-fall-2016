var gulp = require('gulp')
    , paths = require('../../settings/config').gulpPaths
    , gulpLoadPlugins = require('gulp-load-plugins')
    , plugins = gulpLoadPlugins()
    , browserSync = require('browser-sync')
    , reload = browserSync.reload;

gulp.task('js', function () {
	return gulp.src(paths.js.src)
 		.pipe(plugins.concat('site.js'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.js.dest))
    .pipe(reload({stream:true}));
});

gulp.task('jsDev', function () {
	return gulp.src(paths.jsDev.src)
 		.pipe(plugins.concat('site.js'))
    .pipe(plugins.rename({suffix: '.min'}))
    .pipe(gulp.dest(paths.jsDev.dest))
    .pipe(reload({stream:true}));
});

gulp.task('jshint', function () {
    return gulp.src(paths.js.src)
    .pipe(plugins.jshint('.jshintrc'))
    .pipe(plugins.jshint.reporter());
});

gulp.task('js-global', function () {
    gulp.src(paths.js.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsCA_FR/js'))
    gulp.src(paths.js.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsDE/js'))
    gulp.src(paths.js.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsES/js'))
    gulp.src(paths.js.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsFR/js'))
    return gulp.src(paths.js.dest + '/**/*')
    .pipe(gulp.dest('./dist/assetsIT/js'))
});