'use strict';

var gulp = require('gulp')
	  , gulpLoadPlugins = require('gulp-load-plugins')
	  , data = require('../../models/data')
	  , plugins = gulpLoadPlugins()
	  , settings = require('../settings/config')
    , paths = settings.gulpPaths;

gulp.task('production', [
	'js-optimize'
	, 'css-optimize'
	// , 'imgs-optimize'
], function(){
	gulp.start('prod-copy');
});

gulp.task('prod-copy', ['cleanProd'], function() {
  // copy - js
  gulp.src(paths.js.dest + '/*.*')
    .pipe(gulp.dest(paths.productionPath.js))
    .pipe(plugins.filesize());
  // copy - css
  gulp.src(paths.sass.dest + '/*.css')
    .pipe(gulp.dest(paths.productionPath.css))
    .pipe(plugins.filesize());
  // copy - html
  // return gulp.src(paths.ejs.src + 'index.ejs')
  return gulp.src('app/views/index.ejs')
    .pipe(plugins.ejs({
      data: data.en_UK
  		, lang: 'en'
  		, region: 'UK'
  		, production: true
  	}))
    .pipe(plugins.rename({suffix: '-content', extname: '.html'}))
    .pipe(plugins.removeEmptyLines())
    .pipe(gulp.dest(paths.productionPath.html));
});
