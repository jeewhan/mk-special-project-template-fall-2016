'use strict';

var basePublishPath 		= './dist'
    , baseAssetsPath 		= './app/assets/'
    , baseAssetsDevPath 	= './app/assetsDev/'
    , clientNameSpace 		= 'mkors'
    , assetsPath 			= basePublishPath+'/assets/'
    , assetsDevPath 		= basePublishPath+'/assetsDev/';

var webPort 				= 8500
    , browserSyncPort 		= 8501
    , nodemonPort 			= 8502;

module.exports = {
	gulpPaths: {
		nodemon: {
			settings: {
				script: 'index.js',
				ext: 'html js',
				env: { 'NODE_ENV': 'development' },
				ignore: ['node_modules/', 'app/assetsDev/js/*.*', 'app/views/pcs/*.*', 'dist/', 'production/'],
				nodeArgs: ['--debug=' + nodemonPort]
			},
			settingsResponsive: {
				script: 'indexResponsive.js',
				ext: 'html js',
				env: { 'NODE_ENV': 'development' },
				ignore: ['node_modules/', 'app/assetsDev/js/*.*', 'app/views/pcs/*.*', 'dist/', 'production/'],
				nodeArgs: ['--debug=' + nodemonPort]
			},
			port: {
				webPort: webPort
			}
		},
		browserSync: {
			port: browserSyncPort,
			open: false,
			server: {
		        baseDir: basePublishPath,
		        debug: true
		      }
		    // , proxy: 'http://localhost:' + webPort,
		      // ,
			// startPath: "/index.html"
		},
		js: {
			src: [
				baseAssetsPath+'js/lib/**/greensock/**/*.js',
				baseAssetsPath+'js/lib/**/jquery/plugins/*.js',
				baseAssetsPath+'js/lib/**/jquery/**/*.js',
				'!'+baseAssetsPath+'js/lib/**/jquery/jquery-*.js',
				baseAssetsPath+'js/'+clientNameSpace+'/*.js',
				baseAssetsPath+'js/*.js'
			],
			dest: assetsPath+'js'
		},
		jsDev: {
			src: [
				baseAssetsDevPath+'js/*.js',
				'!'+baseAssetsDevPath+'js/responsive/**'
			],
			dest: assetsDevPath+'js'
		},
		jsDevResponsive: {
			src: [
				baseAssetsDevPath+'js/responsive/*.js',
				'!'+baseAssetsDevPath+'js/responsive/vendor.js'
			],
			dest: assetsDevPath+'js'
		},
		pcs: {
			js: {
				src: baseAssetsDevPath+'js/pcs/*.js',
				dest: assetsDevPath+'js/pcs'
			},
			css: {
				src: [
					baseAssetsDevPath+'css/pcs/*.css',
					baseAssetsDevPath+'css/pcs/*.scss'
				],
				dest: assetsDevPath+'css/pcs'
			}
		},
		html:{
			src: [
				'app/views/**/*.html'
			],
			dest: basePublishPath
		},
		ejs:{
			src: [
				'app/views/**/*.ejs',
				'app/views/*.ejs'
			],
			dest: basePublishPath
		},
		css: {
			src: [
				baseAssetsPath+'css/**/*.css',
				baseAssetsPath+'css/*.css'
			],
			dest: assetsPath+'css'
		},
		fonts: {
			src: [
				baseAssetsPath+'css/fonts/*'
			],
			dest: assetsPath+'css/fonts'
		},
		fontsDev: {
			src: [
				baseAssetsDevPath+'css/fonts/*'
			],
			dest: assetsDevPath+'css/fonts'
		},
		fontsDevResponsive: {
			src: [
				baseAssetsDevPath+'css/responsive/fonts/*'
			],
			dest: assetsPath
		},
		sass:{
			src: [
				baseAssetsPath+'css/**/*.scss',
				baseAssetsPath+'css/*.scss'
			],
			dest: assetsPath+'css'
		},
		sassDev:{
			src: [
				baseAssetsDevPath+'css/**/*.scss',
				baseAssetsDevPath+'css/*.scss',
				'!'+baseAssetsDevPath+'css/responsive/**'
			],
			dest: assetsDevPath+'css'
		},
		sassDevResponsive:{
			src: [
				baseAssetsDevPath+'css/responsive/**',
				'!'+baseAssetsDevPath+'css/responsive/fonts/**'
			],
			dest: assetsDevPath+'css'
		},
		imgs: {
			src: [
				baseAssetsPath+'img/**/*',
				baseAssetsPath+'img/*'
			],
			dest: assetsPath+'img'
		},
		imgsCA_FR: {
			src: './app/assetsCA_FR/**/*',
			dest: basePublishPath+'/assetsCA_FR/img'
		},
		imgsDE: {
			src: './app/assetsDE/**/*',
			dest: basePublishPath+'/assetsDE/img'
		},
		imgsES: {
			src: './app/assetsES/**/*',
			dest: basePublishPath+'/assetsES/img'
		},
		imgsFR: {
			src: './app/assetsFR/**/*',
			dest: basePublishPath+'/assetsFR/img'
		},
		imgsIT: {
			src: './app/assetsIT/**/*',
			dest: basePublishPath+'/assetsIT/img'
		},
		imgsDev: {
			src: [
				baseAssetsDevPath+'img/**/*',
				baseAssetsDevPath+'img/*'
			],
			dest: assetsDevPath+'img'
		},
		videos: {
			src: [
				baseAssetsPath+'videos/**/*',
				baseAssetsPath+'videos/*'
			],
			dest: assetsPath+'videos'
		},
		productionPath: {
			js: './production/js',
			css: './production/css',
			html: './production/html'
		},
		sftp: {
			privateKey: '/Users/jkim/Documents/90.Etc/FTP/mkKey_2015Dec_filezilla.ppk',
			stage: {
				host: 'sftp-stage.usi.atg.com',
				port: 22,
				username: 'sftpmks',
				password: 'Michael@Kors',
				dest: {
					css: '/stage11/static/img/MKBusiness/en-US/refreshes/featured/digital-collection-experience-fall-2016/vinyl-ftp-test/css',
					js: '/stage11/static/img/MKBusiness/en-US/refreshes/featured/digital-collection-experience-fall-2016/vinyl-ftp-test/js'
				}
			},
			production: {

			}
		}
	},
};