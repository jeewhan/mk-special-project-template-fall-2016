(function(){

	/**
	* A namespace.
	* @namespace mkors
	*/

  	'use strict';
	mkors = window.mkors || {};

	/**
	* mkors main library on top of all APIs.
	* @function main
	* @memberof mkors
	*/
	mkors.main = function(){
		/* Detect UserAgent */
		var is_not_desktop 	= ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
		var is_mobile 		= ($('.MichaelsMustHaves-mobile').length) ? true: false;
		var is_tablet 		= ($(window).width() > 640) && is_not_desktop ? true: false;
		var is_chrome 		= navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
		var is_safari 		= navigator.userAgent.toLowerCase().indexOf('safari') > -1;
		var is_firefox 		= navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
		
		if((is_chrome)&&(is_safari)) { is_safari = false; }
		if(is_mobile) { is_tablet = false; }

		var $doc = $(document),
			$win = $(window);

		return {
			init: function() {

				/**
				* UK Jenson 2016 Main Modules
				**/

			}
		}
	}();

	mkors.main.init();
	mkors.analytics.init();

})();


(function($){
	/* Header */
	var headerElem = $('header.header-wrapper');
	/* PPH */
	var promoContainer = $('.promo-container');
	/* Navigation */
	var navWrapper = $('.mk-nav-wrapper');
	var navItems = $('.nav-menu-item');
	var mainNav = $('.main-nav-menu');
	$('.main-nav__wrapper').css({'transform':'translateX(0%)'});

	/* For Local Only */
	$('.promo-wrapper, #cls_headerAd').click(function(){
		if(promoContainer.length){
			var expanded = false;

			if(promoContainer.hasClass('expanded')) {
				expanded = true;
			}
			if(expanded) {
				promoContainer.removeClass('expanded');
				promoContainer.addClass('collapsed');
			} else {
				promoContainer.removeClass('collapsed');
				promoContainer.addClass('expanded');
			}

		}
	});

	$('.hamburger').click(function(){
		mainNav.css('height', $(window).height());

		if(navWrapper.length && navItems.length && navWrapper.length){
			var expanded = false;

			if(navWrapper.hasClass('mobile-nav-expanded')) {
				expanded = true;
			}
			if(expanded) {
				navWrapper.removeClass('mobile-nav-expanded');
				navItems.hide();
				$('body').removeClass('mob-menu-expanded');
				
			} else {
				navWrapper.addClass('mobile-nav-expanded');
				navItems.show();
				$('body').addClass('mob-menu-expanded');
			}
		}
	});

	$(window).resize(function(){
		// console.log('resize');
		if($('body').hasClass('mob-menu-expanded')){
			$('.hamburger').click();
			$('.main-nav-menu').css('height', '0');
		}
	});

	$(window).on("scroll", function(){
		if(!headerElem.length) return;

		var winTop = $(window).scrollTop();
		var mainTop = $('main').offset().top;
		if(winTop > mainTop) {
			if(!headerElem.hasClass('scroll')) {
				headerElem.addClass('scroll');
			}
		} else {
			headerElem.removeClass('scroll');
		}
	});
})(jQuery);