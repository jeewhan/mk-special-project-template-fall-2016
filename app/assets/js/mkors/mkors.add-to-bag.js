(function($){
	mkors = window.mkors || {};
	
	mkors.addToBag = function(){
		var galleryList = null;
		var galleryCurrIndex = 0;
		var galleryCollection = null;
		return {
			init: function(){
                
				$(document).on('click', '.quickview_dialog_close, .ui-widget-overlay.ui-front', function(){
					$('#quick_view_popup').dialog().dialog('close');
					if(galleryList != null){
						$('#quick_view_popup').removeClass('gallery-list');
						galleryList = null;
						galleryCurrIndex = 0;
					}
					return false;
				});

				$(document).on('click', '[data-add-to-bag], .colorUpdate', this.openQuickShop);

				$(document).on('click', '.quick_view_list_wrapper [data-add-to-bag]', function(){
					galleryCurrIndex = $(this).index();
					return false;
				});

				$(document).on('keydown', '#product_quickview_popup #addToBag', function(e) {
	                if ((e.which || e.keyCode) == 9 && e.shiftKey && $("#product_quickview_popup .mk_size_guide:visible").length > 0) {
	                    e.preventDefault();
	                    $("#product_quickview_popup .mk_size_guide").focus();
	                }
	            });

	            $(document).on('click', '.product_description_container .changed_input', function() {
		            var $this = $(this);
		            var $parents = $(this).parents(".product_description_container");
		            mkobj.addToBagTextChange($this, $parents);
		        });
			},
			openQuickShop: function(evt){
            	var self = $(this);
            	var target = self.data().targetId || "#quick_view_popup";
            	
            	if((collection = self.data().addToBag) != null && galleryList == null){
            		galleryList = '';
            		var valid = false;
            		galleryList = $('<div class="quick_view_list" />');
            		galleryList.append($('<h3>'+(self.attr('title') || 'Look')+'</h3>'));
            		var galleryListWrap = $('<div class="quick_view_list_wrapper" />');

            		$.each(collection, function(i,n){
            			if((omniSkuId = n.omniSkuId) != "" && (productId = n.productId) != "" && (region = n.region) != ""){
            				
            				var color = n.color || "0001";
            				var imgUrl = "http://michaelkors.scene7.com/is/image/MichaelKors/"+productId+"-"+color+"_1?fit=constrain,1&wid=100&hei=100&fmt=jpg"
            				var url = "/browse/includes/productQuickView.jsp?No=-1&Omniskuid="+omniSkuId+"&productId="+region+"_"+productId+"&isTrends=&nonPrimaryLocale=&selectedColor="+color;
            				var gallertyItem = $('<a href="'+url+'" data-add-to-bag onclick="quickViewOmni(\''+omniSkuId+'\')">');
            				//gallertyItem.data().addToBag = collection;
            				gallertyItem.append($('<img src="'+imgUrl+'"/></a>'));
            				galleryListWrap.append(gallertyItem);

            				if(!valid){
            					valid = true;
            					if(self.attr('href') === undefined || self.attr('href') == "#"){
            						self.attr('href', url);
            					}
            				}
            			}
            			
            		});

					galleryList.append(galleryListWrap);

					if(collection.length == 1){
						galleryList = '';
					}
            	}

            	/*Set The Current URl into The Cookie*/
				mkors.addToBag.setCurrentURLInCookie(self);
				$.ajax({
					url:(self.attr('rel') || self.attr('href')),
					type:'POST',
					dataType: 'html',      
					success: function(data){
						var targetArea = $(target);
						targetArea.html(data);

						if(galleryList != null){
							targetArea.addClass('gallery-list')
								.append(galleryList);

							var childList = targetArea.find('.quick_view_list_wrapper [data-add-to-bag]');
							childList.removeClass('active-item');
							setTimeout(function(){
								childList.eq(galleryCurrIndex).addClass('active-item');
							}, 200);
						}

						if($("#quick_view_popup .product_quickview_popup").length == 0){
							targetArea.dialog({
								autoOpen: false,
								height:'auto',
								width: (galleryList != null)? 1000 : 770,
								modal: true,
								open:function(){
									//mkobj.enable_focus_to_dialog_close_icon(this);
									setTimeout(function(){
										targetArea.find(".ui-state-default.ui-corner-top").attr("tabindex", 0); 
										targetArea.find(".ui-state-default.ui-corner-top").on("click keydown", function(e) {
							                targetArea.find(".ui-state-default.ui-corner-top").attr("tabindex", 0);
							            });
										targetArea.find('.colors_group').find('li a.tooltip, li a img.tooltip, li img.tooltip').tooltipster({
											theme: '.my-custom-theme'
										});
									},100);
								}
							}).dialog('open').dialog('option', 'position', 'center');
						}						
						
						
						targetArea.find('.product_quickview_tabs').tabs(); 
						targetArea.find('.scroll-pane').jScrollPane({
							autoReinitialise: true,                   
							verticalDragMinHeight: 60,
							verticalDragMaxHeight: 60
						});
						
						targetArea.find('select').selectbox().change(function () {
						    console.log($(this).val());
						});
						mkors.addToBag.applyHoverForShare(targetArea);
						
						var chooseSizeText = targetArea.find('#chooseSizeText').val();
						var sizeOption = targetArea.find(".edit_size ").find(".sbSelector").html();

						if( sizeOption != chooseSizeText) {
							targetArea.find(".locatein_store_width").attr("id","locate_store");
						}

						targetArea.find(".edit_size .sbOptions").click(function(){
							if( sizeOption != chooseSizeText) {
								targetArea.find(".store_locator_tooltip").hide();
								targetArea.find(".locatein_store_width").attr("id", "locate_store");
							}
							return false;
						});

						targetArea.find(".locatein_store_width").on('click',function(){
							if( sizeOption != chooseSizeText) {
								mkobj.locateStorePopup();
							}else{
								targetArea.find(".store_locator_tooltip").show();
							}
							return false;
						});
						
						targetArea.find("#locate_store").on('click',function(){
							//mkobj.locateStorePopup();
						});

						// Share Email Popup Showing 
						targetArea.find('.share_email_link').on('click',function(e){
							e.preventDefault();
							mkors.addToBag.showShareEmailPopup(this);
						});

						mkors.addToBag.toolTip('.fav_link', 'right-17 top-15', 'center', 'light-arrow-right');
						mkors.addToBag.toolTip('.favorite_icon', 'right-17 top-15', 'center', 'light-arrow-right');
						

						targetArea.find('.colors_group').find('li a.tooltip, li a img.tooltip, li img.tooltip').tooltipster({
							theme: '.my-custom-theme'
						});

						mkors.addToBag.quickViewPopupArrow(targetArea);
					},
					complete: function(){
						/*alert($("#imagesBox").find(".s7staticimage").length);*/
					}
				});
				
				return false;
            },
			applyHoverForShare: function(targetArea) {
	            targetArea.find(".share_email_div").one("mouseenter", function() {
	                var tempObj = $(this).parent().find(".socialShareDetails");
	                shareProductDetails(tempObj);
	            });

	            targetArea.find(".share_email_div").hover(function() {
	                $(this).find(".share_hover_icons").show();
	            }, function() {
	                $(this).find(".share_hover_icons").hide();
	            });
	        },
			toolTip: function(selector, posMy, posAt, tipClass) {
	            if ($(selector) != null && $(selector).length > 0) {
	                $(selector).tooltip({
	                    position: {
	                        my: posMy,
	                        at: posAt
	                    },
	                    tooltipClass: tipClass,
	                    collision: "none"
	                });
	            }
	        },
			quickViewPopupArrow: function(targetArea) {
	            var $quick_view_popup = targetArea;
	            if ($quick_view_popup.length > 0) {
	                var winWidthN = $(window).width();
	                var divWidth = $quick_view_popup.parent().width();
	                var leftPosN = $quick_view_popup.parent().offset().left;
	                $quick_view_popup.find(".lg-grid-prev").css("left", leftPosN - 40);
	                $quick_view_popup.find(".lg-grid-next").css("left", leftPosN + divWidth + 20);
	            }

	            targetArea.find("select").change(function() {
	                var sel_val = $(this).val().toLowerCase();
	                $(this).next(".sbHolder").find(".sbOptions li a").css("background-color", "#fff").css("color", "#767676");
	                $(this).next(".sbHolder").find('.sbOptions li a[rel="' + sel_val + '"]').css("background-color", "#ccc").css("color", "#767676");
	            });
	        },
	        setCurrentURLInCookie: function(obj) {
	            var $this = $(obj);
	            var name = $this.attr("name");
	            var expires = "expires=-1";
	            var rlCurrentURL;
	            if (name != undefined) {
	                rlCurrentURL = name;
	            } else {
	                rlCurrentURL = $("#rlCurrentURL").val();
	            } if (rlCurrentURL != undefined) {
	                var path = "path=/";
	                document.cookie = "rlCurrentURL" + "=" + rlCurrentURL + "; " + expires + "; path=/;";
	            }
	        },
			showProductQuickviewPopup:function($this, $location){				 	
				$.ajax({
					url:$this.attr('href'),
					type:'POST',
					dataType: 'html',      
					success: function(data){
						if($(data).find('.product_quickview_container').length == 0){
							 $('.single-product-load .out-of-stock-product').show();
						} else {
							$location.html(data);
							$location.find('.colors_group').find('li a.tooltip, li a img.tooltip, li img.tooltip').tooltipster({
								theme: '.my-custom-theme'
							});

							$location.find('select').selectbox();
							mkobj.applyHoverForShare();

							var chooseSizeText = $location.find('#chooseSizeText').val();
							var sizeOption = $location.find(".edit_size").find(".sbSelector").html();
							
							if( sizeOption != chooseSizeText) {
							 	$location.find(".locatein_store_width").attr("id","locate_store");
							}

							$location.find(".edit_size .sbOptions").click(function(){
								var sizeOption = $location.find(".edit_size").find(".sbSelector").html();
								if( sizeOption != chooseSizeText) {
									$location.find(".store_locator_tooltip").hide();
									$location.find(".locatein_store_width").attr("id","locate_store");
								}
							});

							$location.find(".locatein_store_width").on('click',function(){
								var sizeOption = $location.find(".edit_size").find(".sbSelector").html();
								if( sizeOption != chooseSizeText) {
							 	   mkobj.locateStorePopup();
							  	} else{
									$location.find(".store_locator_tooltip").show();
							  	}
							});

							 // Share Email Popup Showing 
							$location.find("#quick_view_popup").find('.share_email_link').on('click',function(e){
								e.preventDefault();
								mkobj.showShareEmailPopup(this);
						   	});
							
						   	mkobj.toolTip('.fav_link', 'right-17 top-15', 'center', 'light-arrow-right');
						   	mkobj.toolTip('.favorite_icon', 'right-17 top-15', 'center', 'light-arrow-right');
						 	//  $('.tooltip').tooltipster('hide');

						   	$location.find('.colors_group').find('li a.tooltip, li a img.tooltip, li img.tooltip').tooltipster({
								theme: '.my-custom-theme'
							});

							//$(document).off('click','.colorUpdate')

							$location.find(".colorUpdate").off('click').on("click",function(e){
	                      	  	e.preventDefault();
	                      	  	var $this = $(this);
	                      	  	mkors.addToBag.showProductQuickviewPopup($this, $location);
	                        });

	                        mkobj.quickViewPopupArrow();
						}
						

						$location.find('.product_quickview_tabs').tabs();
						$location.find('.scroll-pane').jScrollPane({
							autoReinitialise: true,                   
							verticalDragMinHeight: 60,
							verticalDragMaxHeight: 60
						});
					},
					complete: function(){
						/*alert($("#imagesBox").find(".s7staticimage").length);*/
					}
				});
			}
		}

	}();

	if($('[data-add-to-bag]').length > 0){
		mkors.addToBag.init();
	}

	if((addToBag = $('[data-addtobag-embed]')).length > 0){
		addToBag.each(function() {
			var self = $(this);
			mkors.addToBag.showProductQuickviewPopup(self, $(self.data('addtobag-embed')));
		});
	}
})(jQuery);