(function($){

	/**
  * A namespace.
  * @namespace mkors
  */
	mkors = window.mkors || {};
	
	mkors.scrollDown = function(){

		return {
			init: function(scrollDown){
				scrollDown.each(function(){
					var self = $(this);
					var interact = (self.data('interact')!= null)? false : true;

					self.on('click', function(event){
						var scrollOffset = self.attr('data-scroll-offset')? self.attr('data-scroll-offset') : 0;
						var scrollLocation = $(self.attr('href')).offset().top + parseInt(scrollOffset);
						$('html, body').animate({
							scrollTop: scrollLocation,
							easing: 'easeInElastic'
						}, 700);
						if(interact == false){
							interact = true;
							self.delay(500).animate({
								opacity: 0,
							}, 'slow', function(){
								//interact = false;
							});
						}
						event.preventDefault();
					});

					function setFixedAnchor(){
						if(self.data("threshold-type")=="fixed"){
							self.css({
								left: (self.parent().offset().left + self.parent().outerWidth()) - 60
							});
						}
						
					}

					function setScroll(){
						if(interact == false){
							if(thresholdContent = self.attr('data-threshold-limit')){
								var itemOffset = self.offset().top;
								var scrollTopPos = Math.floor($(document).scrollTop() + itemOffset);
								var thresholdContent = $(thresholdContent);
								var navHeight = self.outerHeight();

								//var itemPos = thresholdContent.parent().offset().top + thresholdContent.position().top;
								var itemPos = thresholdContent.parent().offset().top;

								//console.log(itemOffset, itemPos);
								

								if(thresholdContent.length > 0){
									if(itemOffset >= itemPos){
										self.css({
											opacity: 0
										});
									} else {
										self.css({
											opacity: 1
										});
									}
								}
							}
						}
					}
					
					$(window).on("scroll", setScroll)
						.on("resize", setFixedAnchor);

					/*setTimeout(function(){
						setScroll();
					}, 500);*/
					setFixedAnchor();
					self.animate({
						opacity: 1
					}, 'slow');	
				});
			}
		}

	}();

	if((scroll = $('.scroll-down')).length > 0){
		mkors.scrollDown.init(scroll);
	}

})(jQuery);