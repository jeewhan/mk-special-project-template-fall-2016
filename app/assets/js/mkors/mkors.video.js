(function($){

	/**
  * A namespace.
  * @namespace mkors
  */
	mkors = window.mkors || {};
	
	/**
	* Video library on top of YouTubePlayer, Video.js API. (mkors.video).
	* @function video
	* @memberof mkors
	*/
	mkors.video = function(){
		var desktopCheck = (typeof window.orientation !== 'undefined') ? false : true;
		var devMode = false;

		return {
			init: function(){
				if($('[data-video-type="youtube"]').length > 0){
					this.youTubePlayer();
				}
				if($('[data-video-type="html5video"]').length > 0) {
                    $('video').get(0).play();
                    mkors.video.html5videoPlayer();
                }
                if($('[data-video-type="html5video-ctp"]').length > 0) {
                    mkors.video.html5videoClickToPlay();
                }
			},
			youTubePlayer: function(){
				var tag = document.createElement('script');
		
			    tag.src = "https://www.youtube.com/iframe_api";
			    var firstScriptTag = document.getElementsByTagName('script')[0];
			    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			},
			html5videoClickToPlay: function() {
				$('[data-video-type="html5video-ctp"]').each(function(){
					var self = $(this);
					var mobileBg = $('.video-poster');
					var mobileBgBlock = $('.video-mobile-block');
					var parent = $('.jenson-video');
					var videoElm = document.getElementById('jenson-video-elm');
					var $videoElm = $('#jenson-video-elm');

					videoElm.addEventListener('ended', ctpVideoEnded, false);
				    

					parent.find('.playButton').on('click', function(event){
						event.preventDefault();
						mobileBg.css({'opacity': 0});
						mobileBgBlock.show();
						videoElm.play();
						$videoElm.addClass('active');
						$(this).hide();
					});

					function ctpVideoEnded(e) {
						console.log('ctpVideoEnded');
						parent.find('.playButton').show();
						mobileBg.css({'opacity': 1});
						$videoElm.removeClass('active');
				    }
				});

				$('.video-title-mobile').on('click', function(event){
			    	var dataUrl = $(this).attr('data-video-url');
			    	if(dataUrl.length && dataUrl !== 'undefined') {
			    		window.location.href = dataUrl;
			    	}
			    });
			},
			html5videoPlayer: function() {
                $('[data-video-type="html5video"]').each(function(){
                    var self = $(this);
                    var mobileBg = $('.mobileBg');
                    var mobileBgBlock = $('.video-mobile-block');
                    var videoLoop = 0;
                    var videoPlayer = $('video');

                    if(!desktopCheck) {
                        mobileBg.css({'opacity': 1});
                        mobileBg.show();
                        mobileBgBlock.hide();
                        self.hide();
                    } else {
                        mobileBg.hide();
                        mobileBgBlock.show();
                    }

                    self.on("checkLoopEndHTMLVideo", function(){
                        if(devMode) console.log('checkLoopEndHTMLVideo');
                        if ($(window).width() > 1024){
                            self.show()
                                .trigger("resetHTMLVideoPlayer");
                            mobileBg.hide();
                            mobileBgBlock.show();
                        } else {
                            self.hide()
                                .trigger("stopHTMLVideoPlayer");
                            mobileBg.show();
                            mobileBgBlock.hide();
                        }
                        if(!desktopCheck) {
                            mobileBg.show();
                            mobileBgBlock.hide();
                            self.hide();
                        }
                    });
                    self.on("resetHTMLVideoPlayer", function(){
                        if(videoPlayer !== undefined){
                            videoPlayer.get(0).play();
                            if(devMode) console.log('HTML5 Video Play');
                        }
                    });
                    self.on("stopHTMLVideoPlayer", function(){
                        if(videoPlayer !== undefined){
                            videoPlayer.get(0).pause();
                        }
                    });
                });
            }
		};
	}();
	
	mkors.video.init();	

})(jQuery);

function onYouTubePlayerAPIReady() {
	var timer, timeSpent = [];
	var freq = {};
	var devMode = false;
	var desktopCheck = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? false : true;

	$('[data-video-type="youtube"]').each(function(){
		var self = $(this);
		var parent = self.parent().parent();
		var loopPlayer_HTMLVideo = parent.find('[data-video-type="html5video"]');
		var playerAssets = parent.find('[data-video-assets]');
		var mobileBg = $(".video-poster");
		var iframeBg = $('.video-iframe-bg');
		var mobileBgBlock = $('.mobileBg-block');

		self.attr('data-player-status', 'resume');
		
		var done = false;		
		var yTplayer = new YT.Player(self.attr("id"), {
			events: {
	        'onReady': function(event){
	        },
	        'onStateChange': function(event){
		        if (event.data == YT.PlayerState.ENDED) {
		        	mobileBg.show();
		        	mobileBgBlock.hide();
		        	iframeBg.css({'position':'absolute'});
		        	playerAssets.show();

		        	if(loopPlayer_HTMLVideo.length) {
                        self.hide();
                        loopPlayer_HTMLVideo.trigger("checkLoopEndHTMLVideo");
                        playerAssets.show();
                    }
					self.attr('data-player-mode','');
					done = true;          
				} else if (event.data == YT.PlayerState.PLAYING){
					if(loopPlayer_HTMLVideo.length) {
                        playerAssets.hide();
                        self.show();
                    }
		        	self.attr('data-player-mode','active');
		        	done = false;
				} else if (event.data == YT.PlayerState.PAUSED){

				} else if (event.data == YT.PlayerState.UNSTARTED){

				}
					/* Omniture */
				    if(event.data === 1) { // Started playing
				        if(!timeSpent.length){
				            for(var i=0, l=parseInt(yTplayer.getDuration()); i<l; i++) {
				            	timeSpent.push(false);
				            }
				        }
				        timer = setInterval(record,100);
				    } else {
				        clearInterval(timer);
				    }
		        }    
			}
	    });

	    self.on("startYtubePlayer", function(){
			yTplayer.playVideo();
	    });

	    self.on("stopYtubePlayer", function(){
	    	yTplayer.stopVideo();
	    });

	    self.on("pauseYtubePlayer", function(){
	    	yTplayer.pauseVideo();
	    });

	    /* onYouTubeIframeAPIReady, Omniture YouTube Tracking */
		function record(){
		    timeSpent[ parseInt(yTplayer.getCurrentTime()) ] = true;
		    tagRecord();
		}

		function tagRecord(){
		    var percent = 0;
		    for(var i=0, l=timeSpent.length; i<l; i++){
		        if(timeSpent[i]) percent++;
		    }

		    percent = Math.round(percent / timeSpent.length * 100);

		    var percentNew = ((yTplayer.getCurrentTime()) / timeSpent.length)*100;
		    percent = percentNew;
		    if(devMode) {
		    	// console.log(percentNew);
		    	// console.log("tagRecord : " + percent + " %");
		    	// console.log(parseInt(yTplayer.getCurrentTime()) + ' / ' + timeSpent.length);
		    }

		    if(devMode || (typeof s_gi !== undefined && window.mkorsData !== undefined)){
		    	var quadrants = 1;
		    	var events = "event29";

		    	if(percent >= 10 && percent <= 20){
					quadrants = 2;
					events = "event43";
				} else if (percent >= 21 && percent <= 30){
					quadrants = 3;
					events = "event44";
				} else if (percent >= 31 && percent <= 40){
					quadrants = 4;
					events = "event45";
				} else if (percent >= 41 && percent <= 50){
					quadrants = 5;
					events = "event46";
				} else if (percent >= 51 && percent <= 60){
					quadrants = 6;
					events = "event47";
				} else if (percent >= 61 && percent <= 70){
					quadrants = 7;
					events = "event48";
				} else if (percent >= 71 && percent <= 80){
					quadrants = 8;
					events = "event49";
				} else if (percent >= 81 && percent <= 90){
					quadrants = 9;
					events = "event50";
				} else if (percent >= 91 && percent <= 98){
					quadrants = 10;
					events = "event51";
				} else if (percent >= 99){
					quadrants = 11;
					events = "event30";
				}

				if(freq[quadrants] === undefined) {
					freq[quadrants] = true;

					var title = 'JensonCampaign_expernience_NOVFY17';
					var eVar = 'JensonCampaign_expernience_NOVFY17';

					if(!devMode) {
						var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;
						var s = s_gi(s_account);

						s.linkTrackVars = 'eVar40,prop10,events';
						s.linkTrackEvents = events;
						s.prop10 = eVar; /* Global page traffic variables */
						s.eVar40 = eVar;
						s.events = events;
						s.tl(this,'o', title);
						
						// evt.preventDefault();
					}
					if(devMode) {
				    	console.log("JensonCampaign_expernience_NOVFY17 : events: " + events);
				    }
				}
		    }
		}

		if(!desktopCheck) {
			$("#html5video").hide();
			// $('.video-poster').show();
			iframeBg.css({'position':'absolute'});
			mobileBgBlock.hide();
		}
	        
	    parent.find('.playButton').on('click', function(event){
	    	if($('[data-video-type="youtube"]').length > 0){
	    		$('[data-video-type="youtube"]').show();
	    	}
	    	if($(window).outerWidth() <= 675 && !desktopCheck){
	            playerAssets.hide();
	            mobileBg.hide();
	            iframeBg.css({'position':'relative'});
	            mobileBgBlock.show();
	            self.show();
	        } else if(!desktopCheck) {
	            playerAssets.hide();
	            mobileBg.hide();
	            iframeBg.css({'position':'relative'});
	            mobileBgBlock.show();
	            self.show();
	        } else {
	            playerAssets.hide();
	            mobileBg.hide();
	            iframeBg.css({'position':'relative'});
	            mobileBgBlock.show();
	            self.trigger("startYtubePlayer");
	        }
	    	event.preventDefault();
	    });
	});
}
