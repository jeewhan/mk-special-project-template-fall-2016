(function($){
	/**
	* A namespace.
	* @namespace mkors
	*/
	mkors = window.mkors || {};

	/**
	* Analytics library for Adobe Omniture API. (mkors.analytics).
	* @function analytics
	* @memberof mkors
	*/
	mkors.analytics = function(){

		var devMode = false;
        var animateFromMain = false;

		return {	

			init: function(){
				$(document).on('click', 
					'.mmh-d-sticky-nav li a, .mmh-nav-wr [data-omni-evar], .mmh-c-nav-wr [data-omni-evar], .mmh-sticky-nav', 
					function(evt){
		          	var self = $(this);
		                	
					if(typeof s_gi !== undefined && window.mkorsData !== undefined){
						var events = self.attr('data-omni-event');
						var eVar = self.attr('data-omni-evar');
						var title = self.attr('data-omni-title');

						var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;

						var s = s_gi(s_account);
						s.linkTrackVars = 'eVar45,events';
						s.linkTrackEvents = events;
						s.eVar45 = eVar;
						s.events = events;
						s.tl(this,'o', title);

						evt.preventDefault();
					}

					if(devMode) {
						console.log('[omniture Dev]' + self.attr('data-omni-evar'));
					}
				});
				
				/* pctviewed : 26%, 51%, 76% and 100% */
				var freq = {};
				var freq_ch01 = {};
				var freq_ch02 = {};
				var freq_ch03 = {};
				var siteDoc = $(document);
				
				var checkPctViewed = function() {

		      		if(animateFromMain) return;

		      		var docHeight 		= siteDoc.outerHeight();
					var percent 		= Math.ceil(((siteDoc.scrollTop()) / (docHeight - $(window).height())) * 100);
					// var percent100 		= Math.ceil(((siteDoc.scrollTop() + $(window).height()) / docHeight) * 100);

		      		var quadrants 		= mkors.analytics.getQuadrant(percent);

		      		if(freq[quadrants] === undefined && quadrants > 1){

		        		percent = mkors.analytics.getPercentage(quadrants);

			        	if(typeof s_gi !== undefined && window.mkorsData !== undefined) {
			        		freq[quadrants] = true;

							var events = 'event42';
							var title = 'JensonCampaign';
							var eVar = 'JensonCampaign: experience: Scroll: '+percent+'% pctviewed';
							var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;
							var s = s_gi(s_account);

							s.eVar45 = eVar;
							s.events = events;
							s.linkTrackVars = 'eVar45,events';
							s.linkTrackEvents = events;
							s.tl(this,'o', title);
						}

						if(devMode) {
							freq[quadrants] = true;
							console.log('[omniture Dev]' + 'JensonCampaign: experience: Scroll: '+percent+'% pctviewed');
						}
		      		}

		      		// console.log('pecent: ' + percent + ' / percent100: ' + percent100);

		      		var chapter01 = $('#chapter01-panel');
		      		var chapter02 = $('#chapter02-panel');
		      		var chapter03 = $('#chapter03-panel');

		      		if(chapter01.length && chapter01.is(':visible')) {
		      			chapterPctviewed('ElevatedEssentials', chapter01, freq_ch01);
		      		}
		      		if(chapter02.length && chapter02.is(':visible')) {
		      			chapterPctviewed('BlackWhiteBlush', chapter02, freq_ch02);
		      		}
		      		if(chapter03.length && chapter03.is(':visible')) {
		      			chapterPctviewed('EasyOpulence', chapter03, freq_ch03);
		      		}

		      		function chapterPctviewed(chapterName, chapterElem, freq) {
		      			var docHeight = chapterElem.outerHeight();
						var percent 		= Math.ceil(((siteDoc.scrollTop() - chapterElem.offset().top) / (docHeight - $(window).height())) * 100);
			      		// var percent100 		= Math.ceil(((siteDoc.scrollTop() - chapterElem.offset().top + $(window).height()) / docHeight) * 100);

		      			var quadrants 		= mkors.analytics.getQuadrant(percent); 

		      			if(freq[quadrants] === undefined && quadrants > 1){

			        		percent = mkors.analytics.getPercentage(quadrants);

				        	if(typeof s_gi !== undefined && window.mkorsData !== undefined) {
				        		freq[quadrants] = true;

								var events = 'event42';
								var title = 'JensonCampaign';
								var eVar = 'JensonCampaign: experience: ' + chapterName + ': Scroll:  '+percent+'% pctviewed';
								var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;
								var s = s_gi(s_account);

								s.eVar45 = eVar;
								s.events = events;
								s.linkTrackVars = 'eVar45,events';
								s.linkTrackEvents = events;
								s.tl(this, 'o', title);
							}

							if(devMode) {
								freq[quadrants] = true;
								console.log('[omniture Dev]' + 'JensonCampaign: experience: ' + chapterName + ': Scroll: ' + percent + '% pctviewed');
							}
			      		}
		      		}
		      	};

				checkPctViewed();

				/* Audio Tag Tracking */
				var audioClip = $('#collection-audio-clip');
				var audioSwitch_start = false;
				var audioSwitch_end = false;

				if(audioClip.length) {
					audioClip.on('playing', function() {
						if(audioSwitch_start) return;
						audioSwitch_start = true;

						if(typeof s_gi !== undefined && window.mkorsData !== undefined) {
							var events = 'event29';
							var title = 'Trans2016Collection_MayFY17';
							var eVar = 'Trans2016_MayFY17';
							var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;
							var s = s_gi(s_account);

							s.eVar45 = eVar;
							s.prop10 = eVar; /* Global page traffic variables */
							s.events = events;
							s.linkTrackVars = 'eVar40,events';
							s.linkTrackEvents = events;
							s.tl(this,'o', title);
						}

						if(devMode) {
							console.log('[omniture Dev]Audio Start');
						}
					});
				}

				if(audioClip.length) {
					audioClip.on('ended', function() {
						if(audioSwitch_end) return;
						audioSwitch_end = true;

						if(typeof s_gi !== undefined && window.mkorsData !== undefined) {
							var events = 'event30';
							var title = 'Trans2016Collection_MayFY17';
							var eVar = 'Trans2016_MayFY17';
							var s_account = window.mkorsData.tagConfig.adobe.accountVar || s_account;
							var s = s_gi(s_account);

							s.eVar45 = eVar;
							s.prop10 = eVar; /* Global page traffic variables */
							s.events = events;
							s.linkTrackVars = 'eVar40,events';
							s.linkTrackEvents = events;
							s.tl(this, 'o', title);
						}

						if(devMode) {
							console.log('[omniture Dev]Audio 100% Complete');
						}
						
					});
				}
				
				/* Scroll Tracking */
				function scrollHandler() {
					checkPctViewed();
				}

				$(window).scroll(scrollHandler);
			},
			getQuadrant: function(percent) {
				var quadrant = 1;
				if(percent >= 26 && percent <= 50){
	      			quadrant = 2;
	      		} else if (percent >= 51 && percent <= 75){
	      			quadrant = 3;
	      		} else if (percent >= 76 && percent < 100){
	      			quadrant = 4;
	      		} else if (percent >= 100) {
	      			quadrant = 5;
	      		}
	      		return quadrant;
			},
			getPercentage: function(quadrant) {
				var ret = 0;
				switch(quadrant) {
        			case 2: ret = '26'; break;
        			case 3: ret = '51'; break;
        			case 4: ret = '76'; break;
        			case 5: ret = '100'; break;
        		}
        		return ret;
			},
			getAnimateStatus: function(boolean) {
				animateFromMain = boolean;
			}
		}
	}();
	// * Fixed: mkors.analytics.init() should be called after main.js
	// mkors.analytics.init();	
})(jQuery);