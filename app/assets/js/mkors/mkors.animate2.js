(function($){
	/**
	* A namespace.
	* @namespace mkors
	*/
	mkors = window.mkors || {};

	/**
	* Animate library on top of Greensock and scrollmagic API. (mkors.animate2).
	* ver 1.5.0
	* @function animate2
	* @memberof mkors
	*/
	mkors.animate2 = function(){

		var devTest = false;
		var is_mobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
		var is_tablet = ($(window).width() > 640) ? true: false;
		var is_chrome = (/chrome/.test(navigator.userAgent.toLowerCase()));
		var is_safari = navigator.userAgent.indexOf("Safari") > -1;
		var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
		var is_mavericks = false;
		if((is_chrome)&&(is_safari)) { is_safari = false; }
		if(is_tablet && !is_mobile) { is_tablet = false; }

		if($('.MichaelsMustHaves-mobile').length) {
			is_mobile = true;
			is_tablet = false;
		}

		var mobileTest = false;

		return {
			tweenElems: [],
			assets: null,
			tweenController: null,
			init: function(){
				if($('.lazy').length > 0){
					$('img.lazy').show().lazyload({
		             	event: 'lazyload', effect: 'fadeIn', threshold : 600
		           	}).trigger('lazyload');
					$('div.lazy').show().lazyload({
		             	event: 'lazyload', effect: 'show', threshold : 0
		           	}).trigger('lazyload');
				}
				if($('#mkBodyTemplate-mobile') !== undefined && 
					$('#mkBodyTemplate-mobile').length > 0) {
					is_mobile = true;
				}

				this.tweenController = new ScrollMagic.Controller();
				this.getTweenAssets();
			},
			getTweenAssets: function() {
				// console.log(navigator.userAgent);
				if(mobileTest) return;
				
				if(is_mobile && !is_tablet) {
					this.assets = mkors.animateAssets.mobileAssets;
				} else {
					// Desktop & tablet
					this.assets = mkors.animateAssets.assets;
				}
				if(this.assets != null) {
					this.parseTweenAssets();
				}
			},
			parseTweenAssets: function() {
				var c = 0;
				for(t in this.assets) {
					if(t == 'tweenTo' || t == 'classToggle') {
						c++;	
					}
					switch(t) {
						case 'tweenTo':
							// Parallax Animation Version 1
							// ex: Holiday Gift Guide 2015
							var p = this.assets.tweenTo;
							for(var i=0; i<p.length; i++) {
								var tl = new TimelineMax();
								if(devTest) console.log(p[i]);
								this.setAnimations(tl, p[i]);
							}
						break;
						case 'classToggle':
							// Parallax Animation Version 2
							// ex: Valentine's Day Gift Guide 2016
							var p = this.assets.classToggle;
							for(var i=0; i<p.length; i++) {

								var tl = new TimelineMax();
								if(devTest) console.log(p[i]);
								this.setClassAnimations(tl, p[i]);
							}
						break;
						default:
						break;
					}

				}
				if(c == 0) {
					return;
				}
			},
			setAnimations: function(tl, param) {
				var self		= param.self,
					animParam 	= param.animate || {},
					timeParam 	= parseFloat(param.time || 0),
					tween 		= null,
					sceneParam	= {};

				if(is_tablet && param.tabletDisable) {
					return;
				}

				if(is_firefox && param.firefoxDisable) {
					return;
				}

				if(is_safari && param.safariDisable) {
					return;
				}

				var sceneId = 'scene_id_' + (new Date().getTime()) + '_' + (Math.random()).toString().replace(/\./g,'');
				$(self).attr('data-scene-id', sceneId);
				// console.log(sceneId);

				if(param.animate) {
					animParam.ease 	= param.animate.ease || 'Linear.easeNone';	
				} else {
					animParam.ease 	= 'Linear.easeNone';
				}
				
				var tweenElems = mkors.animate2.tweenElems;

				tweenElems[sceneId] = {};
				tweenElems[sceneId].animProps = animParam;
				tweenElems[sceneId].timeDuration = timeParam;
				tweenElems[sceneId].animProps.ease = animParam.ease;

				sceneParam.triggerHook 		= param.triggerHook || 'onLeave';
				sceneParam.triggerElement 	= param.target_id || '';
				sceneParam.tweenChanges 	= true;
				sceneParam.reverse 			= param.reverse;
				sceneParam.duration			= param.duration || 0;
				sceneParam.offset			= param.offset || 0;

				tweenElems[sceneId].scrollMagic = new ScrollMagic.Scene(sceneParam);

				if(param.indicators){
					tweenElems[sceneId].scrollMagic.addIndicators();
				}	
				// if(classToggle = param.classToggle){
				// 	scene.setClassToggle(classToggle.target, classToggle.className);
				// }

				function updateSceneValues(e) {
					var duration = parseFloat(param.duration.replace(/%/g, '')) / 100.0;
					// durationValue = Math.ceil((parseFloat((param.duration || '100%').replace(/%/g, '')) / 100.0));
					// offsetValue = ((parseFloat(param.offset || 0) / 100.0) * $(param.target_id).offset().top);
					// console.log(duration, $(self).offset().top);
					// sceneParam.offset = offsetValue;
					sceneParam.duration = duration;
				}
				updateSceneValues();

				$(window).resize(function(){
					updateSceneValues();
				});
				// /trend/collection-mailer-2016-trans/_/R-cat670004
				/* Parallax adding and removing */
				$(self).on("scene-remove", function(e){
					// console.log('scene-remove');
					var sceneIdOriginal = $(e.target).attr('data-scene-id');
					var sceneId = $(this).data().sceneId;
					if(sceneIdOriginal !== sceneId) return;
					tweenElems[sceneId].scrollMagic.removeTween(true);
				});
				
				$(self).on("scene-add", function(e){
					// validate element is not the parent's
					var sceneIdOriginal = $(e.target).attr('data-scene-id');
					var sceneId = $(this).data().sceneId;
					if(sceneIdOriginal !== sceneId) return;
					// console.log("scene-add: " + sceneId);
					tweenElems[sceneId].scrollMagic.removeTween(true);
					
					if(tweenElems[sceneId].animProps){	
						tween = TweenMax.to(self, tweenElems[sceneId].timeDuration, tweenElems[sceneId].animProps);
						tl.add(tween);
					}
										
					tweenElems[sceneId].scrollMagic.addTo(mkors.animate2.tweenController);

					if(tween){
						// console.log('scene-add - id:' + sceneId + ' ' + tweenElems[sceneId].animProps);
						tweenElems[sceneId].scrollMagic.setTween(tween);
						tweenElems[sceneId].scrollMagic.refresh();
					}
					
					if(tweenElems[sceneId].animProps.indicators){
						// console.log('indi');
						tweenElems[sceneId].scrollMagic.addIndicators();
					}

					// mkors.animate2.tweenController.enabled();
				});
			},
			setClassAnimations: function(tl, param) {
				var self		= param.self,
					animParam	= param.animateClassToggle || {},
					sceneParam = {};

				if(is_tablet && param.tabletDisable) {
					return;
				}

				if(is_firefox && param.firefoxDisable) {
					return;
				}

				if(is_safari && param.safariDisable) {
					return;
				}

				animParam.ease = param.ease || 'Linear.easeNone';

				sceneParam.triggerHook 		= param.triggerHook || 'onLeave';
				sceneParam.triggerElement 	= param.triggerElement || '';
				sceneParam.tweenChanges 	= true;
				sceneParam.reverse 			= param.reverse || true;

				for(var i=0; i<animParam.classCount; i++) {
					var scene = new ScrollMagic.Scene(sceneParam);
					var duration;

					if(i+1 >= animParam.classCount) {
						if(animParam.classDurationLast == "100%") {
							duration = $(self).outerHeight();
						} else {
							duration = parseFloat(animParam.classDurationLast) || 0;
						}
					} else {
						if(animParam.classDuration == "100%") {
							duration = $(self).outerHeight();
						} else {
							duration = parseFloat(animParam.classDuration) || 0;
						}
					}

					var classSurfix;
					if(animParam.classCount <= 1) {
						classSurfix = '';
					} else {
						classSurfix = i+1;
					}

					scene.addTo(mkors.animate2.tweenController);
					scene.setClassToggle(animParam.target, animParam.className + classSurfix);

					var offsetValue;
					if(animParam.classOffset == "100%") {
						offsetValue = $(param.triggerElement).outerHeight();
					} else {
						offsetValue = parseFloat(param.offset || 0) + ((animParam.classOffset || 0) * i);
					}

					scene.offset(offsetValue);
					scene.duration(duration);

					if(param.indicators){
						if(param.indicatorOptions) {
							var iName 		= param.indicatorOptions.name + (i+1) || 'Start' + (i+1);
							var iColorStart = param.indicatorOptions.colorStart || '#000';
							var iColorEnd 	= param.indicatorOptions.colorEnd || '#000';
							scene.addIndicators({name: iName, colorStart: iColorStart, colorEnd: iColorEnd});
						} else {
							scene.addIndicators();
						}
					}

					// scene.enabled(false);
				}
			}


		}
	}();
	mkors.animate2.init();
})(jQuery);