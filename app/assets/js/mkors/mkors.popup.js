(function($){

	/**
  * A namespace.
  * @namespace mkors
  */
	mkors = window.mkors || {};
	
	mkors.popup = function(){
		var animationEvents = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

		return {
			init: function(popupContent){
				var tempAccordStore = null;

				popupContent.each(this.launchPopup);
				
			},
			launchPopup: function(){
			   	var self = $(this);
			   	//var currentSlider = self.attr('data-slide-stop');
				var target = $('[data-launch-target="'+self.attr('data-launch-popup')+'"]');

				var className = ($(window).width() <= 640)? "show-mobile-pop" : "show-pop";

				target.find('img.lazy').show().lazyload({
					effect : "fadeIn",
					event: "sporty",
				    threshold : 200
				});
				
				self.off('click')
					.on('click', popupOpen);

				target.find('a.close, .popup-bg')
					.off('click')
					.on('click', popupClose);	

				function popupOpen(evt){
					target.show();
					target.find("img.lazy").trigger("sporty");
					target.find(".popup-bg").addClass("animated show-bg")
						.show()
						.one(animationEvents, function(){
							target.find(".popup-wrapper")
							.addClass("animated "+className+" show-pop zoomIn")
							.show()
							.one(animationEvents, function(){
								$('body').addClass('popup-overflow');
								if(videoPlayer = target.find("[data-video-type=\"youtube\"]")){
									videoPlayer.trigger("startYtubePlayer");
								}
							});
						});

					evt.preventDefault();
				}
					
				function popupClose(evt){
					$('body').removeClass('popup-overflow');
					if(videoPlayer = target.find("[data-video-type=\"youtube\"]")){
						videoPlayer.trigger("stopYtubePlayer");
					}

					target.find(".popup-wrapper")
						.removeClass("show-pop zoomIn")
						.addClass("animated "+className+" zoomOut")
						.show()
						.one(animationEvents, function(){
							$(this).hide()
								.removeClass("animated "+className+" zoomOut");

							target.find(".popup-bg").removeClass("show-bg").addClass("animated hide-bg")
							.show()
							.one(animationEvents, function(){
								$(this).hide()
									.removeClass("animated hide-bg");
								
								target.hide();
							});
						});

					evt.preventDefault();
				}

			}
		};
	}();

	if((popup = $('[data-launch-popup]')).length > 0){
		mkors.popup.init(popup);
	}

})(jQuery);