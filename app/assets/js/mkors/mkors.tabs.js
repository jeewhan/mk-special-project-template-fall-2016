(function($){

	/**
  * A namespace.
  * @namespace mkors
  */
	mkors = window.mkors || {};
	
	mkors.tabs = function(){

		return {
			init: function(tabs){
				tabs.each(function(){
					var self = $(this);
					self.find('[data-tab-target]').on('click', function(evt){
						$('[data-tab-trigger]').hide();

						var target = $(this);
						target.closest('.tabs')
							.find('.tab')
							.removeClass('active');

						$('[data-tab-trigger="'+target.data('tab-target')+'"]').show();
						target.closest('.tab').addClass('active');
						evt.preventDefault();
					});

					var index = self.data('tabs');
					index =  index? index : 0;

					self.find('[data-tab-target]').eq(index).trigger('click');
				});
			}
		}

	}();

	if($('[data-tabs]').length > 0){
		mkors.tabs.init($('[data-tabs]'));
	}

})(jQuery);