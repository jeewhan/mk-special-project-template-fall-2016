(function($){
	/**
	* A namespace.
	* @namespace mkors
	*/
	mkors = window.mkors || {};

	mkors.animateAssets = function() {
		return {
			assets: {
				tweenTo: [
					/* Main Contents */
		    	],
		    	classToggle: [
		    	]
		    },
		    mobileAssets: {
		    	tweenTo: [
		    	],
		    	classToggle: [
		    	]
		    }
		}
	}();
})(jQuery);