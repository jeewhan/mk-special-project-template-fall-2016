(function(){

    /**
    * A namespace.
    * @namespace mkors
    */
    mkors = window.mkors || {};

    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = 
          window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
 
    if (!window.requestAnimationFrame)
    window.requestAnimationFrame = function(callback, element) {
        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
    };
 
    if (!window.cancelAnimationFrame)
    window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
    };

    mkors.sequence = (function(){
        var fps = 30;
        return {
            init: function(sequence){
                if(!window['performance']) {
                    window.performance = window.performance || {} ; 
                    window.performance.now = window.performance.now || 
                    (function()  { 
                        var st =Date.now();
                        return function() {
                            return Date.now() - st;
                        } 
                    })() ; 
                }
                sequence.each(function(){
                    var self = $(this);
                    var delay = self.data('delay-frames') || 100;
                    
                    setTimeout(function(){
                        var fps          = self.data('fps') || 30,
                            currentFrame = 0,
                            totalFrames  = self.data('image-frames'),
                            loopFrames  = self.data('loop-frames'),
                            currentTime  = mkors.sequence.rightNow();

                        (function animloop(time){
                            var delta = (time - currentTime) / 1000;
                            var restartFrame = true;

                            currentFrame += (delta * fps);

                            var frameNum = Math.floor(currentFrame);

                            if (frameNum >= totalFrames) {
                                if(loopFrames !== undefined){
                                    restartFrame = loopFrames;
                                }
                                currentFrame = frameNum = 0;
                            }

                            if(restartFrame){
                                requestAnimationFrame(animloop);

                                self.css({
                                    'background-position': "center -" + (frameNum * self.outerHeight()) + "px"
                                });

                                currentTime = time;
                            }
                        })(currentTime);
                    }, delay);
                });
            },
            rightNow: function(){
                if (window['performance'] && window['performance']['now']) {
                    return window['performance']['now']();
                } else {
                    if(window.performance && window.performance.now) {
                        return window.performance.now;
                    } else {
                       return +(new Date()); 
                    }
                }
            }
        };
    })();
    if((sequence = $('[data-image-sequence]')).length > 0){
        mkors.sequence.init(sequence);
    }
})();

// Nav - Video Page Execption Temp
// var navOffset = $(".slideout-menu-toggle, nav#c-menu--slide-left, #mkshare, #c-mask, #hidenav").offset().top - 30;
// $(window).scroll(function() {
//     var scrollPos = $(window).scrollTop();
//     if (scrollPos >= navOffset) {
//         $(".slideout-menu-toggle, nav#c-menu--slide-left, #mkshare, #c-mask, #hidenav").addClass("fixed");
//     } else {
//         $(".slideout-menu-toggle, nav#c-menu--slide-left, #mkshare, #c-mask, #hidenav").removeClass("fixed");
//     }
// });