(function($){
	/**
	* A namespace.
	* @namespace mkors
	*/
	mkors = window.mkors || {};
	
	mkors.slideShow = function(){
		var animationEvents = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

		return {
			init: function(slideShow){

				slideShow.each(function(){
					var cycleSlide = $(this);
					var aniFadeIn, aniFadeOut = false;
					var hideDelayTimer = null;
					var threshold = 1024;
					var arrows = null;

					cycleSlide.find('img.lazy').show().lazyload({
						effect : "fadeIn",
						event: "sporty",
					    threshold : 200
					});
					
					if ($(window).width() > threshold){
						
						if(cycleSlide.parent().find("a.carousel-next, a.carousel-prev")){
							arrows = cycleSlide.parent().find("a.carousel-next, a.carousel-prev");
						} else {
							arrows = cycleSlide.find("a.carousel-next, a.carousel-prev");
						}
						arrows.hide();						
					}

			 		function triggerAnimation(event, optionHash, slideOptionsHash, currentSlideEl) {
						var allChildren = $(currentSlideEl).find('[data-animation-stagger]');
						
						$(currentSlideEl).find("img.lazy").trigger("sporty");

						allChildren.each(function(i){
							var self = $(this);
							var className = self.data('animation-stagger');
							className = className? className :  "fadeIn";
							setTimeout(function(){
								self.addClass("animated "+className)
									.show()
									.one(animationEvents, function(){

									});
							}, i * 300)
							
						})
					}
					triggerAnimation();

					

					cycleSlide.on('cycle-after', triggerAnimation);
					cycleSlide.on('cycle-paused', function(event, optionHash) {
						if ($(window).width() > threshold){
							var className = "animated fadeInArrow";

							if(hideDelayTimer) clearTimeout(hideDelayTimer);

							if(!aniFadeIn && !aniFadeOut){
								aniFadeIn = true;
								arrows.addClass(className)
									.show()
									.one(animationEvents, function(){
									$(this).removeClass(className);
								});
							}
						}
					}).on('cycle-resumed', function(event, optionHash) {
						if ($(window).width() > threshold){
							var className = "animated fadeOutArrow";
							if(hideDelayTimer) clearTimeout(hideDelayTimer);

							//aniFadeIn = false;
							//if(!aniFadeOut){
								//aniFadeOut = true;
								hideDelayTimer = setTimeout(function(){
									hideDelayTimer = null;
									aniFadeOut = true;
									aniFadeIn = false;
										arrows.addClass(className)
										.show()
										.one(animationEvents, function(){
											$(this).hide().removeClass(className);
											aniFadeOut = false;
										});
								}, 800)
								
							//}
						}
					});	
					
					if(carousel = cycleSlide.data('carousel-bind')){
						cycleSlide.on('cycle-next cycle-prev', function(e, opts) {
						    cycleSlide.not(this).cycle('goto', opts.currSlide);
						});

						var carouselNav = $(carousel);

						carouselNav.find('.cycle-slide').click(function(){
						    var index = carouselNav.data('cycle.API').getSlideIndex(this);
						    cycleSlide.cycle('goto', index);
						});	
					}
					
					
					$(document).keydown(function(e) {
						//console.log(e.pageX, e.pageY);

					    switch(e.which) {
					        case 37: // left
					        	cycleSlide.cycle('prev');
					        break;
					
					        case 38: // up
					        break;
					
					        case 39: // right
					        	cycleSlide.cycle('next');
					        break;
					
					        case 40: // down
					        break;
					
					        default: return; // exit this handler for other keys
					    }
					    e.preventDefault(); // prevent the default action (scroll / move caret)
					});
				});
			}
		}

	}();

	if((slider = $('.slideshows-slider')).length > 0){
		mkors.slideShow.init(slider);
	}

})(jQuery);