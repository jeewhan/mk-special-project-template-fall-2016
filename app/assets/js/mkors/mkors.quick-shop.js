(function($){

	/**
  * A namespace.
  * @namespace mkors
  */
	mkors = window.mkors || {};
	
	mkors.quickShop = function(){
		var animationEvents = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

		return {
			init: function(shopElem){
                
                $(document).on('click', shopElem, function(evt){
                	var self = $(this);

                	var className = ($(window).width() <= 640)? "show-mobile-pop" : "show-pop";

                	if(self.attr('data-submit') == null || self.attr('data-submit') == "false"){
                		self.attr('data-submit', true);
	                	$.ajax({
							url: self.data('launch-quickshop'),
							type:'POST',
							dataType: 'html',      
							success: function(data){
								//console.log(data);
								var target = null;
								var isAlreadyOpened = false;
								self.attr('data-submit', false);
								if($('[data-launch-target="'+this.url+'"]').length > 0){
									target = $('[data-launch-target="'+this.url+'"]');
									isAlreadyOpened = true;
									target.html('');
								} else {
									target = $('<div>').addClass('popup-hide');
									target.attr('data-launch-target', this.url);
								}
								
								target.append($('<div>').addClass('popup-bg'));

								//var popupWrapper = $('<div>').addClass('popup-wrapper splash_landing');
								//popupWrapper.attr('data-quick-shop', '');


								var contentData = $(data);
								target.append(contentData);
								target.append($('<a>').attr('href', '#').addClass('close')); 

								target.find('#product_quickview_popup').addClass('popup-wrapper splash_landing')
								.attr('data-quick-shop', 'jetmaster');

								target.find('a.close, .popup-bg')
									.off('click')
									.on('click', popupClose);      

								$('body').append(target);   


								mkobj.enable_tab_for_ui_tabs(); 
								mkobj.override_ui_tab_default_index(this);

								target.find('li a.tooltip, li a img.tooltip, li img.tooltip').tooltipster({
									theme: '.my-custom-theme'
								});

								mkobj.custom_remove_javascript_void();

								target.find('.product_quickview_tabs').tabs(); 
								target.find('.scroll-pane').jScrollPane({
									autoReinitialise: true,                   
									verticalDragMinHeight: 60,
									verticalDragMaxHeight: 60
								});

								var chooseSizeText = target.find('#chooseSizeText').val();
								var sizeOption = target.find(".edit_size").find(".sbSelector").html();
								
								if( sizeOption != chooseSizeText) {
								 	target.find(".locatein_store_width").attr("id","locate_store");
								}

								target.find(".edit_size .sbOptions").on('click', function(evt){
									var sizeOption = target.find(".edit_size").find(".sbSelector").html();
									if( sizeOption != chooseSizeText) {
										target.find(".store_locator_tooltip").hide();
									 	target.find(".locatein_store_width").attr("id","locate_store");
									}
									evt.preventDefault();
								});

								target.find(".locatein_store_width").on('click',function(evt){
									var sizeOption = target.find(".edit_size").find(".sbSelector").html();
									if( sizeOption != chooseSizeText) {
									   	mkobj.locateStorePopup();
								  	} else{
										$(".store_locator_tooltip").show();
								  	}
								  	evt.preventDefault();
								});
									 // Share Email Popup Showing 
								target.find("#quick_view_popup").find('.share_email_link').on('click', function(evt){
									evt.preventDefault();
									mkobj.showShareEmailPopup(this);
							   	});
									
							   	mkobj.toolTip('.fav_link', 'right-17 top-15', 'center', 'light-arrow-right');
							   	mkobj.toolTip('.favorite_icon', 'right-17 top-15', 'center', 'light-arrow-right');
								
							   	mkobj.quickViewPopupArrow();
							   	//mkobj.attch_fav_add_or_remove();
								  
								target.find('.colors_group li.selected a').trigger('click');

								target.find('.colorUpdate').off('click').on("click",function(e){
		                      	  	e.preventDefault();
		                      	  	self.trigger('click');
		                        });

								function popupOpen(evt){
									target.show();
									//popupHide.find("img.lazy").trigger("sporty");
									target.find(".popup-bg").addClass("animated show-bg")
										.show()
										.one(animationEvents, function(){
											target.find(".popup-wrapper")
											.addClass("animated "+className+" show-pop zoomIn")
											.show()
											.one(animationEvents, function(){
												$('body').addClass('popup-overflow');
												
											});
										});
	    						}

	    						if(!isAlreadyOpened){
	    							popupOpen();
	    						}

	    						function popupClose(evt){
									$('body').removeClass('popup-overflow');
									target.find(".popup-wrapper")
										.removeClass("show-pop zoomIn")
										.addClass("animated "+className+" zoomOut")
										.show()
										.one(animationEvents, function(){
											$(this).hide()
												.removeClass("animated "+className+" zoomOut");

											target.find(".popup-bg").removeClass("show-bg").addClass("animated hide-bg")
											.show()
											.one(animationEvents, function(){
												$(this).hide()
													.removeClass("animated hide-bg");
												
												target.hide().remove();
											});
										});

									evt.preventDefault();
								}
							},
							complete: function(){
								
							}
						});
					}
					evt.preventDefault();
                });
            }
        };
	}();

	if($('[data-launch-quickshop]').length > 0){
		mkors.quickShop.init('[data-launch-quickshop]');
	}
})(jQuery);