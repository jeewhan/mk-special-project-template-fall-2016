'use strict';
var genAllSwitch = false;
var pcsActive = false;
var pcsStatus = 0; // 0: section-generate, 1: section-deploy, 2: section-debug, 3:section-options
var configData = {};
var configChanged = false;

var generateSuccess = function(msg, id){
	return '<div class="alert alert-success"  style="display:none;" role="alert" id="'+ id +'"><strong>Completed!</strong> ' + msg + '</div>';
};

var generateFailure = function(msg, id){
	return '<div class="alert alert-danger"  style="display:none;" role="alert" id="'+ id +'"><strong>Failure!</strong> ' + msg + '</div>';
};

var genSuccessHandler = function(res){
	var randId = new Date().getTime();
	var alertBox = generateSuccess('The Final <strong>' + res.file + '</strong> has been generated to: ' + res.path, randId);

	$(alertBox).insertAfter('#gen-js-prg');
	$('#' + randId).fadeIn().delay(3000).fadeOut(600, function(){ $(this).remove(); });
	if(!genAllSwitch) pcsActive = false;
};

var depSuccessHandler = function(res){
	var randId = new Date().getTime();
	var alertBox = generateSuccess('<strong>' + res.file + '</strong> has been deployed to: ' + res.path, randId);
	$(alertBox).insertAfter('#dep-js-prg');
	$('#' + randId).fadeIn().delay(5000).fadeOut(600, function(){ $(this).remove(); });
	pcsActive = false;
};

var errorHandler = function(type){
	var randId = new Date().getTime();
	var alertBox = generateFailure(type + ' / Failure! Please check the console logs in Terminal', randId);
	$(alertBox).insertAfter('#gen-js-prg');
	$('#' + randId).fadeIn().delay(4000).fadeOut(600, function(){$(this).remove();});
};

function progressBar(elm, from, to){

	var progressContinue = function(percentage, time){
		setTimeout(function() {
			if(!pcsActive) return;
			$('#' + elm).find('.progress-bar').css('width', percentage);
		}, 200 * time);
	};

	for(var i=from; i<=to; i+=1) {
		progressContinue(i + '%', i-from);
	}
}

function genSuccess(res){
	if(res === undefined || res === null) {
		$('#gen-css-prg, #gen-js-prg, #gen-html-prg, #gen-all-prg').delay(500).fadeOut();
		$('#gen-css').removeClass('btn-default').addClass('btn-info');
		pcsActive = false;
		errorHandler('undefined');
	}
	if(res.type === 'css') {
		if(res.return === undefined) {
			$('#gen-css-prg').delay(500).fadeOut();
			$('#gen-css').removeClass('btn-default').addClass('btn-info');
			pcsActive = false;
			errorHandler('CSS');
		} else {
			if(res.step === '1') {
				postGenerate(res.type, 2);
				if(genAllSwitch) {
					$('#gen-all-prg').show().find('.prg-all-css').css('width', '10%');
				}
				else {
					$('#gen-css-prg').show().find('.progress-bar').css('width', '50%');	
				}
			}
			if(res.step === '2') {
				if(genAllSwitch) {
					$('#gen-all-prg').show().find('.prg-all-css').css('width', '25%');
					postGenerate('html', 1);
					genSuccessHandler(res);
				}
				else {
					$('#gen-css').removeClass('btn-default').addClass('btn-info');
					$('#gen-css-prg').show().find('.progress-bar').css('width', '100%');
					$('#gen-css-prg').delay(500).fadeOut(600);
					genSuccessHandler(res);
				}
			}
		}
	}
	if(res.type === 'html') {
		if(res.return === undefined) {
			$('#gen-html-prg').delay(500).fadeOut();
			$('#gen-html').removeClass('btn-default').addClass('btn-warning');
			pcsActive = false;
			errorHandler('HTML');
		} else {
			if(res.step === '1') {
				postGenerate(res.type, 2);
				if(genAllSwitch) {
					$('#gen-all-prg').show().find('.prg-all-html').css('width', '10%');
				} else {
					$('#gen-html-prg').show().find('.progress-bar').css('width', '50%');
				}
			}
			if(res.step === '2') {
				if(genAllSwitch) {
					$('#gen-all-prg').show().find('.prg-all-html').css('width', '25%');
					postGenerate('js', 1);
					genSuccessHandler(res);
				}
				else {
					$('#gen-html').removeClass('btn-default').addClass('btn-warning');
					$('#gen-html-prg').show().find('.progress-bar').css('width', '100%');
					$('#gen-html-prg').delay(500).fadeOut(600);
					genSuccessHandler(res);
				}
			}
		}
	}
	if(res.type === 'js') {
		if(res.return === undefined) {
			$('#gen-js-prg').delay(500).fadeOut();
			$('#gen-js').removeClass('btn-default').addClass('btn-success');
			pcsActive = false;
			errorHandler('JS');
		} else {
			if(res.step === '1') {
				postGenerate(res.type, 2);
				if(genAllSwitch) {
					$('#gen-all-prg').show().find('.prg-all-js').css('width', '10%');
				} else {
					$('#gen-js-prg').show().find('.progress-bar').css('width', '50%');
					progressBar('gen-js-prg', 50, 90);
				}
			}
			if(res.step === '2') {
				if(genAllSwitch) {
					$('#gen-all').removeClass('btn-default').addClass('btn-primary');
					$('#gen-all-prg').show().find('.prg-all-js').css('width', '50%');
					$('#gen-all-prg').delay(500).fadeOut(600);
					genAllSwitch = false;
					genSuccessHandler(res);
				} else {
					$('#gen-js').removeClass('btn-default').addClass('btn-success');
					$('#gen-js-prg').show().find('.progress-bar').css('width', '100%');
					$('#gen-js-prg').delay(500).fadeOut(600);
					genSuccessHandler(res);
				}
			}
		}
	}
	
}

function depSuccess(res){
	if(res.type === 'css') {
		if(res.return === undefined) {
			$('#dep-css-prg').delay(500).fadeOut();
			$('#dep-css').removeClass('btn-default').addClass('btn-info');
			pcsActive = false;
			errorHandler('CSS');
		} else {
			$('#dep-css').removeClass('btn-default').addClass('btn-info');
			$('#dep-css-prg').show().find('.progress-bar').css('width', '100%');
			$('#dep-css-prg').delay(500).fadeOut(600);
			depSuccessHandler(res);
		}
	}
	if(res.type === 'js') {
		if(res.return === undefined) {
			$('#dep-js-prg').delay(500).fadeOut();
			$('#dep-js').removeClass('btn-default').addClass('btn-info');
			pcsActive = false;
			errorHandler('JS');
		} else {
			$('#dep-js').removeClass('btn-default').addClass('btn-warning');
			$('#dep-js-prg').show().find('.progress-bar').css('width', '100%');
			$('#dep-js-prg').delay(500).fadeOut(600);
			depSuccessHandler(res);
		}
	}
}

function validateSuccess(res){
	pcsActive = false;
	if(res.type === 'css' || res.type === 'js' || res.type === 'html') {
		if(res.return === undefined) {
			console.log('validate failure');
		} else {
			// console.log('validateSuccess : ' + res.type);
			$('#val-result').html(res.return);
			$('#val-result').fadeIn();

			if(res.type === 'css') {
				$('#val-css').addClass('btn-info').removeClass('btn-default');
			}
			if(res.type === 'html') {
				$('#val-html').addClass('btn-warning').removeClass('btn-default');
			}
			if(res.type === 'js') {
				$('#val-js').addClass('btn-success').removeClass('btn-default');
			}
		}
	}
}

var getConfig = function(){
	$.ajax({
		type: 'POST',
		url: '/getconfig',
		success: function(data){
			configData = data;
			setConfigData();
		},
		dataType: 'json',
		timeout: 3000
	});
};

var updateConfig = function(){
	// console.log(configData);
	$.ajax({
		type: 'POST',
		url: '/setconfig',
		data: JSON.stringify(configData),
		success: function(data){
			// console.log(data);
			configChanged = false;
			$('#options-save').addClass('disabled');
		},
		error: function(data){
			console.log('updateConfig Fails:' + data);
		},
		contentType: "application/json",
		dataType: 'json',
		timeout: 3000
	});	
}

function postGenerate(type, step){
	var timeout = type==='js' ? 12500:5000;
	$.ajax({
	  type: 'POST',
	  url: '/generate',
	  data: {
	  	type: type,
	  	step: step
	  },
	  success: genSuccess,
	  error: function(){
	  		$('#gen-css-prg, #gen-js-prg, #gen-html-prg, #gen-all-prg').delay(500).fadeOut();
		  	pcsActive = false;
			errorHandler(type);
		},
	  dataType: 'json',
	  timeout: timeout
	});
}

function postShowFiles(type) {
	$.ajax({
		type: 'POST',
		url: '/show',
		data: {type: type},
		success: null,
		dataType: 'json',
		timeout: 20000
	});
}

function postDeploy(type){
	$.ajax({
	  type: 'POST',
	  url: '/deploy',
	  data: {
	  	type: type
	  },
	  success: depSuccess,
	  dataType: 'json',
	  timeout: 50000
	});
}

function validateFiles(type) {
	$.ajax({
		type: 'POST',
		url: '/validate',
		data: {
			type: type
		},
		success: validateSuccess,
		dataType: 'json',
		timeout: 50000
	});
}

function setConfigData(){
	if(configData.sftp.privateKey !== undefined) {
		$('#upload-file-info').html(configData.sftp.privateKey);	
	}
	if(configData.sftp.stage.host !== undefined) {
		$('#stg-host').val(configData.sftp.stage.host);	
	}
	if(configData.sftp.stage.username !== undefined) {
		$('#stg-username').val(configData.sftp.stage.username);	
	}
	if(configData.sftp.stage.password !== undefined) {
		$('#stg-password').val(configData.sftp.stage.password);	
	}
}

$(function(){
	$('#nav-generate').click(function(){
		if(pcsActive) return;
		if(pcsStatus === 0) return;
		pcsStatus = 0;
		$(this).addClass('active').siblings().removeClass('active');
		$('#section-generate').show().siblings('section').hide();
	});
	$('#nav-deploy').click(function(){
		if(pcsActive) return;
		if(pcsStatus === 1) return;
		pcsStatus = 1;
		$(this).addClass('active').siblings().removeClass('active');
		$('#section-deploy').show().siblings('section').hide();
	});
	$('#nav-debug').click(function(){
		if(pcsActive) return;
		if(pcsStatus === 2) return;
		pcsStatus = 2;
		$(this).addClass('active').siblings().removeClass('active');
		$('#section-debug').show().siblings('section').hide();
	});
	$('#nav-options').click(function(){
		if(pcsActive) return;
		if(pcsStatus === 3) return;
		pcsStatus = 3;
		$(this).addClass('active').siblings().removeClass('active');
		$('#section-options').show().siblings('section').hide();
	});

	$('#nav-options').click(); // for test

	// section-generate
	$('#gen-all').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#gen-all').removeClass('btn-primary').addClass('btn-default');
		$('#gen-all-prg').show().find('.progress-bar').css('width', '0');
		genAllSwitch = true;
		postGenerate('css', 1);
	});
	$('#gen-css').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#gen-css').removeClass('btn-info').addClass('btn-default');
		$('#gen-css-prg').show().find('.progress-bar').css('width', '0');
		postGenerate('css', 1);
	});
	$('#gen-js').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#gen-js').removeClass('btn-success').addClass('btn-default');
		$('#gen-js-prg').show().find('.progress-bar').css('width', '0');
		postGenerate('js', 1);
	});
	$('#gen-html').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#gen-html').removeClass('btn-warning').addClass('btn-default');
		$('#gen-html-prg').show().find('.progress-bar').css('width', '0');
		postGenerate('html', 1);
	});
	$('#show-css').click(function(){
		if(pcsActive) return;
		postShowFiles('css');
	});
	$('#show-js').click(function(){
		if(pcsActive) return;
		postShowFiles('js');
	});
	$('#show-html').click(function(){
		if(pcsActive) return;
		postShowFiles('html');
	});

	// section-deploy
	$('#dep-css').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#dep-css').removeClass('btn-info').addClass('btn-default');
		$('#dep-css-prg').show().find('.progress-bar').css('width', '0');
		postDeploy('css');
	});
	$('#dep-js').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#dep-js').removeClass('btn-warning').addClass('btn-default');
		$('#dep-js-prg').show().find('.progress-bar').css('width', '0');
		postDeploy('js');
	});
	$('#server-selector').change(function(){
		var arr = $('#dist-folder').val().split('/');
		var dist = '/' + $(this).val();

		for(var i=2; i<arr.length; i+=1) {
			dist += '/' + arr[i];
		}
		$('#dist-folder').val(dist);
	});

	// section-debug
	$('#val-css').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$(this).removeClass('btn-info').addClass('btn-default');
		validateFiles('css');
	});
	$('#val-html').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$(this).removeClass('btn-warning').addClass('btn-default');
		validateFiles('html');
	});
	$('#val-js').click(function(){
		if(pcsActive) return;
		pcsActive = true;
		$('#val-result').fadeOut();
		$(this).removeClass('btn-success').addClass('btn-default');
		validateFiles('js');
	});

	// section-options
	$('#pcs-fileloader').change(function(){
		configChanged = true;
		$('#options-save').removeClass('disabled');

		var ppkFile = $(this).val();

		$('#upload-file-info').html(ppkFile);
		configData.sftp.privateKey = ppkFile;
	});
	$('#options-save').click(function(){
		if($(this).hasClass('disabled')) return;
		// console.log('save');
		$(this).addClass('disabled');
		pcsActive = true;
		updateConfig();
	});

	getConfig();
});

function valDetails(elem){
	var contents = $(elem).parent().next();
	if(contents.is(':visible')) {
		$(elem).parent().next().fadeOut();
	} else {
		$(elem).parent().next().fadeIn();	
	}
}

function clickFileloader(server) {
	$('#pcs-fileloader').click();
}
