(function(){
	var express		= require('express');
	var model = require('../models/data');

	module.exports = function(app) {
		var router = express.Router();
		/* Query Strings: localhost:port/?lang=fr&region=ca&production=false&mobile=false */
		router.get('/', function(req, res) {
		    res.render('indexResponsive.ejs', {
                data			: model.en_UK
                , lang 			: req.query.lang
                , region		: req.query.region
                , production 	: req.query.prod
                , promo			: req.query.promo
		    });
		})
		app.use('/', router);
	}
})();