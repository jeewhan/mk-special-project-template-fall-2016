module.exports = {
    en_UK: {
        baseUrl: '/img/MKBusiness/en-UK/specialProjects/jenson-campaign',
        section_a: {
            header: 'OFF DUTY',
            header_m: 'OFF DUTY',
            text: 'The legendary McLaren-Honda brand and its world<br/>renowned drivers are an exceptional representation<br/>of the Michael Kors man. To celebrate the lifestyle<br/>partnership, we collaborated with British GQ<br/>to shoot 2009 Formula 1 World Champion and<br/>McLaren-Honda driver Jenson Button.',
            text_m: 'The legendary McLaren-Honda brand and its<br/>world renowned drivers are an exceptional<br/>representation of the Michael Kors man.<br/>To celebrate the lifestyle partnership, we<br/>collaborated with British GQ to shoot 2009<br/>Formula 1 World Champion and McLaren-<br/>Honda driver Jenson Button.'
        },
        section_d: {
            text: 'Fitting with the laid-back sophistication of the season,<br/>Michael Kors has created 50 limited-edition men’s<br/>leather jackets featuring both the Michael Kors and<br/>McLaren logos, as well as a bespoke plaque with each<br/>piece’s unique production number.',
            text_m: 'Fitting with the laid-back sophistication of<br/>the season, Michael Kors has created 50<br/>limited-edition men’s leather jackets featuring<br/>both the Michael Kors and McLaren logos, as<br/>well as a bespoke plaque with each piece’s<br/>unique production number.',
            cta: 'SHOP THE LOOKS'
        },
        section_e: {
            text: 'From a ready-to-wear range of impeccably<br/>tailored casual wear to stylish footwear, we<br/>captured Jenson Button wearing the latest<br/>trends from Michael Kors. Explore the jackets,<br/>sweaters and sneakers as modelled by the<br/>iconic racing driver.',
            text_m: 'From a ready-to-wear range of impeccably<br/>tailored casual wear to stylish footwear, we<br/>captured Jenson Button wearing the latest<br/>trends from Michael Kors. Explore the jackets,<br/>sweaters and sneakers as modelled by the<br/>iconic racing driver.',
            cta: 'SHOP THE LOOKS'
        },
        section_g: {
            text: 'From his favourite Karaoke tune to his dream garage, Jenson answers<br/>questions in an exclusive video captured behind the scenes.',
            text_m: 'From his favourite Karaoke tune to his dream<br/>garage, Jenson answers questions in an<br/>exclusive video captured behind the scenes.'
        },
        linking_d: {
            image1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image3: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            CTA1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image4: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            CTA2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image5: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            video: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image6: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote3: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb'
        },
        linking_m: {
            Header: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            CTA1: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image3: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image4: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image5: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote3: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            CTA2: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            video: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            quote4: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb',
            image6: '/trend/michael-kors-mclaren-honda/_/N-1g5qqsb'
        }
    },
    tagData: {

    }
};