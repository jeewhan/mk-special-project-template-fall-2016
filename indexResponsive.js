var express		= require('express.io')
    , app = express()
    , bodyParser = require('body-parser')
    , path = require('path')
    , paths = require('./app/config/settings/config').gulpPaths
    , port = process.env.PORT || paths.nodemon.port.webPort;

require('./app/routes/appResponsive')(app);

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use(express.compress());
app.use("/", express.static(path.join(__dirname, 'dist')));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './app/views'));

app.listen(port);
